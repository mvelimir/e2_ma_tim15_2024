package com.eventplanner.repositories;

import com.eventplanner.model.Notification;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class NotificationRepository {
    private static final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private static CollectionReference notificationCol = db.collection("notifications");

    public static void create(Notification notification){
        DocumentReference notificationRef = notificationCol.document();
        notification.setId(notificationRef.getId());

        notificationRef.set(notification);
    }

    public static void markAsRead(String id) {
        if(id == null){
            return;
        }
        DocumentReference notificationRef = notificationCol.document(id);

        notificationRef.update("unread", false);
    }

    public static void getNotificationsForUser(String userId, Consumer<ArrayList<Notification>> consumer) {
        ArrayList<Notification> notifications = new ArrayList<>();

        notificationCol
            .whereEqualTo("userId", userId)
            .whereEqualTo("unread", true)
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Notification notification = document.toObject(Notification.class);
                        notifications.add(notification);
                    }

                    consumer.accept(notifications);
                }
            });
    }

    public static void getAllNotificationsForUser(String userId, Consumer<ArrayList<Notification>> consumer) {
        ArrayList<Notification> notifications = new ArrayList<>();

        notificationCol
                .whereEqualTo("userId", userId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Notification notification = document.toObject(Notification.class);
                            notifications.add(notification);
                        }

                        consumer.accept(notifications);
                    }
                });
    }

}
