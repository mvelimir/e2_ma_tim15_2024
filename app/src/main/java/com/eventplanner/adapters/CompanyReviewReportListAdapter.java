package com.eventplanner.adapters;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.eventplanner.R;
import com.eventplanner.model.CompanyReview;
import com.eventplanner.model.CompanyReviewReport;
import com.eventplanner.model.CompanyReviewReportStatus;
import com.eventplanner.model.Notification;
import com.eventplanner.model.User;
import com.eventplanner.model.UserType;
import com.eventplanner.repositories.NotificationRepository;
import com.eventplanner.services.CompanyReviewReportService;
import com.eventplanner.services.CompanyReviewService;
import com.eventplanner.services.UserService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CompanyReviewReportListAdapter extends ArrayAdapter<CompanyReviewReport> {
    public CompanyReviewReportListAdapter(FragmentActivity activity, ArrayList<CompanyReviewReport> reports) {
        super(activity, 0, reports);
        navController = Navigation.findNavController(activity, R.id.nav_host_fragment_content_main);
    }

    NavController navController;

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        CompanyReviewReport report = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.company_review_report_card, parent, false);
        }

        convertView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("ownerId", report.getOwnerId());

            navController.navigate(R.id.nav_owner_profile, bundle);
        });

        UserService userService = new UserService();
        CompanyReviewReportService service = new CompanyReviewReportService();
        CompanyReviewService reviewService = new CompanyReviewService();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        TextView owner = convertView.findViewById(R.id.owner);
        TextView reason = convertView.findViewById(R.id.reason);
        TextView date = convertView.findViewById(R.id.date);
        TextView status = convertView.findViewById(R.id.status);
        Button btnAccept = convertView.findViewById(R.id.accept_button);
        Button btnReject = convertView.findViewById(R.id.reject_button);

        reason.setText(report.getReason());
        date.setText(dateFormat.format(report.getDate()));
        status.setText(report.getStatus().toString());
        userService.get(report.getOwnerId(), u -> {
            owner.setText(u.getFirstName() + " " + u.getLastName());
        });

        if (report.getStatus() != CompanyReviewReportStatus.REPORTED) {
            btnAccept.setVisibility(View.GONE);
            btnReject.setVisibility(View.GONE);
        }

        btnAccept.setOnClickListener(v -> {
            btnAccept.setVisibility(View.GONE);
            btnReject.setVisibility(View.GONE);

            report.setStatus(CompanyReviewReportStatus.ACCEPTED);
            service.update(report);
            reviewService.delete(report.getReviewId());

            status.setText(report.getStatus().toString());
        });

        btnReject.setOnClickListener(v -> {
            showCustomDialog(report.getOwnerId(), () -> {
                btnAccept.setVisibility(View.GONE);
                btnReject.setVisibility(View.GONE);

                report.setStatus(CompanyReviewReportStatus.REJECTED);
                service.update(report);

                status.setText(report.getStatus().toString());
            });
        });

        return convertView;
    }

    private void showCustomDialog(String ownerId, Runnable runnable) {
        LayoutInflater inflater = LayoutInflater.from(getContext());

        View dialogView = inflater.inflate(R.layout.dialog_company_review_report_rejection, null);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());

        dialogBuilder.setView(dialogView);

        EditText reason = dialogView.findViewById(R.id.reason_field);

        dialogBuilder.setPositiveButton("Confirm", (dialog, which) -> {
            String reasonStr = reason.getText().toString();
            if (reasonStr.isEmpty()) return;

            runnable.run();

            Notification n = new Notification();
            n.setMessage("Your company review report has been rejected for the following reason: '" + reasonStr + "'");
            n.setTitle("Rejection of company review report");
            n.setUserId(ownerId);
            n.setDate(new Date());

            NotificationRepository.create(n);
        });

        dialogBuilder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }
}
