package com.eventplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.eventplanner.R;
import com.eventplanner.model.Notification;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class NotificationListAdapter extends ArrayAdapter<Notification> {
    private FragmentActivity activity;

    public NotificationListAdapter(FragmentActivity activity, ArrayList<Notification> notifications) {
        super(activity, 0, notifications);
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Notification notification = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.notification_card, parent, false);
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        TextView title = convertView.findViewById(R.id.title);
        TextView message = convertView.findViewById(R.id.message);
        TextView date = convertView.findViewById(R.id.date);

        title.setText(notification.getTitle());
        message.setText(notification.getMessage());
        date.setText(dateFormat.format(notification.getDate()));

        return convertView;
    }
}
