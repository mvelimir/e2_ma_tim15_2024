package com.eventplanner.adapters;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.DialogInterface;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.eventplanner.R;
import com.eventplanner.model.CompanyReview;
import com.eventplanner.model.CompanyReviewReport;
import com.eventplanner.model.Notification;
import com.eventplanner.model.Reservation;
import com.eventplanner.model.User;
import com.eventplanner.model.UserType;
import com.eventplanner.repositories.NotificationRepository;
import com.eventplanner.services.CompanyReviewReportService;
import com.eventplanner.services.CustomerServiceService;
import com.eventplanner.services.EmployeeEventService;
import com.eventplanner.services.UserService;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CompanyReviewListAdapter extends ArrayAdapter<CompanyReview> {

    private String ownerId;

    public CompanyReviewListAdapter(FragmentActivity activity, ArrayList<CompanyReview> reviews) {
        super(activity, 0, reviews);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        CompanyReview review = getItem(position);

        final String reviewId = review.getId();

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.company_review_card, parent, false);
        }

        UserService userService = new UserService();
        CompanyReviewReportService reportService = new CompanyReviewReportService();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        TextView rating = convertView.findViewById(R.id.rating);
        TextView comment = convertView.findViewById(R.id.comment);
        TextView date = convertView.findViewById(R.id.date);
        TextView organizer = convertView.findViewById(R.id.organizer);
        Button btnReport = convertView.findViewById(R.id.report_button);

        rating.setText(new Integer(review.getRating()).toString());
        comment.setText(review.getComment());
        date.setText(dateFormat.format(review.getDate()));

        userService.get(review.getOrganizerId(), u -> {
            organizer.setText(u.getFirstName() + " " + u.getLastName());
        });

        userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
            User user = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class);

            String rId = getItem(position).getId();

            if (user.getType() == UserType.OWNER) {
                ownerId = user.getId();

                reportService.getByReviewId(getItem(position).getId(), r -> {
                    if (r != null) {
                        btnReport.setEnabled(false);
                    }
                });

                btnReport.setOnClickListener(v -> {
                    showCustomDialog(rId, btnReport);
                });
            } else {
                btnReport.setVisibility(View.GONE);
            }
        });

        return convertView;
    }

    private void showCustomDialog(String reviewId, Button btnReport) {
        LayoutInflater inflater = LayoutInflater.from(getContext());

        View dialogView = inflater.inflate(R.layout.dialog_company_review_report, null);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());

        dialogBuilder.setView(dialogView);

        EditText reason = dialogView.findViewById(R.id.report_field);

        dialogBuilder.setPositiveButton("Confirm", (dialog, which) -> {
            CompanyReviewReportService service = new CompanyReviewReportService();

            if (reason.getText().toString().isEmpty()) return;

            CompanyReviewReport report = new CompanyReviewReport();
            report.setReason(reason.getText().toString());
            report.setReviewId(reviewId);
            report.setOwnerId(ownerId);

            service.create(report);
            btnReport.setEnabled(false);

            Notification n = new Notification();

            UserService userService = new UserService();

            n.setTitle("Report of company review");
            n.setMessage("A company review has been reported for the following reason: '" + reason.getText().toString() + "'");
            n.setDate(new Date());
            n.setUnread(true);

            userService.forEachAdminId(id -> {
                n.setUserId(id);

                NotificationRepository.create(n);
            });
        });

        dialogBuilder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }
}
