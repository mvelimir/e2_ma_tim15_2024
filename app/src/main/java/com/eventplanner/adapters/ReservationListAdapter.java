package com.eventplanner.adapters;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.icu.util.BuddhistCalendar;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.model.Employee;
import com.eventplanner.model.Reservation;
import com.eventplanner.model.ReservationStatus;
import com.eventplanner.model.User;
import com.eventplanner.model.UserType;
import com.eventplanner.services.CustomerServiceService;
import com.eventplanner.services.EmployeeEventService;
import com.eventplanner.services.ReservationService;
import com.eventplanner.services.UserService;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReservationListAdapter extends ArrayAdapter<Reservation> implements View.OnClickListener {
    private FragmentActivity activity;
    private NavController navController;

    public ReservationListAdapter(FragmentActivity activity, ArrayList<Reservation> reservations) {
        super(activity, 0, reservations);
        this.activity = activity;
        navController = Navigation.findNavController(this.activity, R.id.nav_host_fragment_content_main);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Reservation reservation = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.reservation_card, parent, false);
        }

        Bundle bundle = new Bundle();
        bundle.putString("reservationId", reservation.getId());

        convertView.setOnClickListener(view -> navController.navigate(R.id.nav_reservation, bundle));

        EmployeeEventService employeeEventService = new EmployeeEventService();
        CustomerServiceService customerServiceService = new CustomerServiceService();
        UserService userService = new UserService();
        ReservationService service = new ReservationService();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat fullDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
;
        ImageView icon = convertView.findViewById(R.id.icon);
        TextView serviceName = convertView.findViewById(R.id.service_name);
        TextView reservationStatus = convertView.findViewById(R.id.reservation_status);
        TextView eventDate = convertView.findViewById(R.id.event_date);
        TextView eventStartTime = convertView.findViewById(R.id.event_start_time);
        TextView eventEndTime = convertView.findViewById(R.id.event_end_time);
        TextView organizedBy = convertView.findViewById(R.id.organized_by);
        TextView forEmployee = convertView.findViewById(R.id.for_employee);
        TextView withCancellationUntil = convertView.findViewById(R.id.with_cancellation_until);
        Button btnCancel = convertView.findViewById(R.id.cancel_button);
        Button btnAccept = convertView.findViewById(R.id.accept_button);

        btnCancel.setVisibility(VISIBLE);
        btnCancel.setEnabled(true);
        btnAccept.setVisibility(VISIBLE);
        btnAccept.setEnabled(true);

        if (reservation.isPartOfPackage()) {
            icon.setColorFilter(getColor(reservation.getPackageId()));
        } else {
            icon.setVisibility(View.GONE);
        }

        if (reservation.getStatus() != ReservationStatus.NEW && reservation.getStatus() != ReservationStatus.ACCEPTED) {
            btnCancel.setVisibility(GONE);
        }

        if (reservation.getCancellationTimeLimit().before(new Date())) {
            btnCancel.setEnabled(false);
        } else {
            btnCancel.setEnabled(true);
        }

        if (reservation.getFrom().before(new Date())) {
            btnAccept.setEnabled(false);
        } else {
            btnAccept.setEnabled(true);
        }

        if (reservation.getStatus() == ReservationStatus.ACCEPTED) {
            btnAccept.setVisibility(GONE);
        } else {
            btnAccept.setVisibility(VISIBLE);
        }

        if (reservation.getStatus() == ReservationStatus.CANCELLED_BY_ADMIN) {
            btnCancel.setVisibility(GONE);
            btnAccept.setVisibility(GONE);
        }

        if (reservation.getStatus() == ReservationStatus.CANCELLED_BY_ORGANIZER) {
            btnCancel.setVisibility(GONE);
            btnAccept.setVisibility(GONE);
        }

        if (reservation.getStatus() == ReservationStatus.CANCELLED_BY_EMPLOYEE) {
            btnCancel.setVisibility(GONE);
            btnAccept.setVisibility(GONE);
        }

        if (reservation.getStatus() == ReservationStatus.REALIZED) {
            btnCancel.setVisibility(GONE);
            btnAccept.setVisibility(GONE);
        }

        btnAccept.setOnClickListener(v -> {
            service.accept(reservation, this::notifyDataSetChanged);
        });


        userService.getUser(u -> {
            btnCancel.setOnClickListener(v -> {
                service.cancel(reservation, u.getType(), true, this::notifyDataSetChanged);
            });

            if (u.getType() != UserType.EMPLOYEE && u.getType() != UserType.OWNER) {
                btnAccept.setVisibility(GONE);
            }
        });

        customerServiceService.get(reservation.getServiceId(), s -> {
            serviceName.setText(s.getName());
        });
        reservationStatus.setText(reservation.getStatus().toString().replace("_", " "));
        withCancellationUntil.setText("With deadline at " + fullDateFormat.format(reservation.getCancellationTimeLimit()));

        eventDate.setText(dateFormat.format(reservation.getFrom()));
        eventStartTime.setText(timeFormat.format(reservation.getFrom()));
        eventEndTime.setText(timeFormat.format(reservation.getTo()));

        if (reservation.getEmployeeId() != null) {
            userService.get(reservation.getEmployeeId(), u -> {
                forEmployee.setText("For " + u.getFirstName() + " " + u.getLastName());
            });
        }
        if (reservation.getOrganizerId() != null) {
            userService.get(reservation.getOrganizerId(), u -> {
                organizedBy.setText("Organized by " + u.getFirstName() + " " + u.getLastName());
            });
        }

        return convertView;
    }

    @Override
    public void onClick(View v) {

    }

    private int getColor(String input) {
        byte[] hash = getHash(input);

        int r = (hash[0] & 0xFF);
        int g = (hash[1] & 0xFF);
        int b = (hash[2] & 0xFF);

        return Color.rgb(r, g, b);
    }

    private byte[] getHash(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(input.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("No such hashing algorithm", e);
        }
    }
}
