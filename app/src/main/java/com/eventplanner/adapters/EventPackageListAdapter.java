package com.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eventplanner.R;
import com.eventplanner.model.EventPackage;
import com.eventplanner.model.Product;
import com.eventplanner.ui.eventPackages.EventPackageListFragment;

import java.util.ArrayList;

public class EventPackageListAdapter extends ArrayAdapter<EventPackage> {

    private ArrayList<EventPackage> aPackages;

    public EventPackageListAdapter(Context context, ArrayList<EventPackage> packages){
        super(context, R.layout.package_card, packages);
        aPackages = packages;

    }

    @Override
    public int getCount() {
        return aPackages.size();
    }

    @Nullable
    @Override
    public EventPackage getItem(int position) {
        return aPackages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        EventPackage eventPackage = getItem(position);

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.package_card, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.packageCard = convertView.findViewById(R.id.package_card_item);
            viewHolder.textName = convertView.findViewById(R.id.text_name);
            viewHolder.textDescription = convertView.findViewById(R.id.text_description);
            viewHolder.textPrice = convertView.findViewById(R.id.text_price);
            viewHolder.textProducts = convertView.findViewById(R.id.text_products);
            viewHolder.textCustomerServices = convertView.findViewById(R.id.text_customer_services);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (eventPackage != null) {
            viewHolder.textName.setText("Name: " + eventPackage.getName());
            viewHolder.textDescription.setText("Description: " + eventPackage.getDescription());
            viewHolder.textPrice.setText("Price: " + eventPackage.getPrice());
            viewHolder.textProducts.setText("Products: " + eventPackage.getProducts());
            viewHolder.textCustomerServices.setText("Services: " + eventPackage.getCustomerServices());

            viewHolder.packageCard.setOnClickListener(v -> {
                // Handle click event
                Navigation.findNavController(v).navigate(R.id.eventPackageDetails);
                Toast.makeText(getContext(), "Category Clicked: " + eventPackage.getCategory(), Toast.LENGTH_SHORT).show();
            });
        }

        return convertView;
    }


    private static class ViewHolder {
        LinearLayout packageCard;
        TextView textName;
        TextView textDescription;
        TextView textPrice;
        TextView textProducts;
        TextView textCustomerServices;

    }
}
