package com.eventplanner.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;


import com.eventplanner.R;
import com.eventplanner.model.CustomerService;

import java.util.ArrayList;

public class CustomerServiceListAdapter extends ArrayAdapter<CustomerService> {
    private ArrayList<CustomerService> aServices;

    public CustomerServiceListAdapter(Context context, ArrayList<CustomerService> services){
        super(context, R.layout.service_card, services);
        aServices = services;

    }

    @Override
    public int getCount() {
        return aServices.size();
    }

    @Nullable
    @Override
    public CustomerService getItem(int position) {
        return aServices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CustomerService service = getItem(position);

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.service_card, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.serviceCard = convertView.findViewById(R.id.service_card_item);
            viewHolder.textName = convertView.findViewById(R.id.text_name_service);
            viewHolder.textDescription = convertView.findViewById(R.id.text_description_service);
            viewHolder.textPrice = convertView.findViewById(R.id.text_price_service);
            viewHolder.textLocation = convertView.findViewById(R.id.text_location_service);
            viewHolder.textDuration = convertView.findViewById(R.id.text_duration_service);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (service != null) {
            viewHolder.textName.setText("Name: " + service.getName());
            viewHolder.textDescription.setText("Description: " + service.getDescription());
            viewHolder.textPrice.setText("Full price: " + service.getFullPrice());
            viewHolder.textLocation.setText("Location: " + service.getLocation());
            viewHolder.textDuration.setText("Duration: " + service.getDuration());

            viewHolder.serviceCard.setOnClickListener(v -> {

                Navigation.findNavController(v).navigate(R.id.serviceDetailsFragment);

                Toast.makeText(getContext(), "Category Clicked: " + service.getCategory(), Toast.LENGTH_SHORT).show();
            });

        }

        return convertView;
    }

    private static class ViewHolder {
        LinearLayout serviceCard;
        TextView textName;
        TextView textDescription;
        TextView textPrice;
        TextView textLocation;
        TextView textDuration;
    }
}
