package com.eventplanner.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;


import com.eventplanner.R;
import com.eventplanner.model.Product;

import java.util.ArrayList;

public class ProductListAdapter extends ArrayAdapter<Product> {
    private ArrayList<Product> aProducts;

    public ProductListAdapter(Context context, ArrayList<Product> products){
        super(context, R.layout.product_card, products);
        aProducts = products;

    }

    @Override
    public int getCount() {
        return aProducts.size();
    }

    @Nullable
    @Override
    public Product getItem(int position) {
        return aProducts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Product product = getItem(position);

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_card, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.productCard = convertView.findViewById(R.id.product_card_item);
            viewHolder.textName = convertView.findViewById(R.id.text_name);
            viewHolder.textDescription = convertView.findViewById(R.id.text_description);
            viewHolder.textPrice = convertView.findViewById(R.id.text_price);
            viewHolder.textEventType = convertView.findViewById(R.id.text_event_type);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (product != null) {
            viewHolder.textName.setText("Name: " + product.getName());
            viewHolder.textDescription.setText("Description: " + product.getDescription());
            viewHolder.textPrice.setText("Price: " + product.getPrice());
            viewHolder.textEventType.setText("Event type: " + product.getEventType());

            viewHolder.productCard.setOnClickListener(v -> {

                Navigation.findNavController(v).navigate(R.id.productDetailsFragment);

                Toast.makeText(getContext(), "Category Clicked: " + product.getCategory(), Toast.LENGTH_SHORT).show();
            });

        }

        return convertView;
    }

    private static class ViewHolder {
        LinearLayout productCard;
        TextView textName;
        TextView textDescription;
        TextView textPrice;
        TextView textEventType;
    }
}
