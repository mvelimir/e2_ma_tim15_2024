package com.eventplanner.services;

import com.eventplanner.model.EventOrganizer;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

public class EventOrganizerService {
    private FirebaseFirestore db;

    public EventOrganizerService(){
        db = FirebaseFirestore.getInstance();
    }

    public Task<Void> SaveEventOrganizer(EventOrganizer eventOrganizer)
    {
        return db.collection("eventOrganizers").document().set(eventOrganizer);
    }
}
