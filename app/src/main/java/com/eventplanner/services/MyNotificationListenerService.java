package com.eventplanner.services;

import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.eventplanner.repositories.NotificationRepository;

public class MyNotificationListenerService extends NotificationListenerService {

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
        String notificationId = sbn.getTag();

        NotificationRepository.markAsRead(notificationId);
    }
}
