package com.eventplanner.services;

import android.util.Log;

import com.eventplanner.model.CustomerService;
import com.eventplanner.model.Reservation;
import com.eventplanner.model.User;
import com.eventplanner.model.UserType;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Document;

import java.sql.Array;
import java.util.ArrayList;
import java.util.function.Consumer;

public class UserService {
    private FirebaseAuth auth;
    private FirebaseFirestore db;

    public static String email;
    public static String password;

    public UserService() {
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    public boolean isLoggedIn() {
        return auth.getCurrentUser() != null;
    }

    public Task<QuerySnapshot> getUser() {
        String email = auth.getCurrentUser().getEmail();
        //String password = auth.getCurrentUser().
        return db.collection("users").whereEqualTo("email", email).get();
    }

    public void getUser(Consumer<User> consumer) {
        String email = auth.getCurrentUser().getEmail();

        db.collection("users").whereEqualTo("email", email).get().addOnSuccessListener(queryDocumentSnapshots -> {
            User user = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class);

            consumer.accept(user);
        });
    }

    public <T extends User> void get(String id, Class<T> c, Consumer<T> consumer) {
        db.collection("users").document(id).get().addOnSuccessListener(documentSnapshot -> {
            T user = documentSnapshot.toObject(c);

            consumer.accept(user);
        });
    }

    public void getOwnerIdFromCompanyId(String companyId, Consumer<String> consumer) {
        db.collection("users").whereEqualTo("companyId", companyId).get().addOnSuccessListener(queryDocumentSnapshots -> {
            for (DocumentSnapshot document : queryDocumentSnapshots) {
                String id = document.toObject(User.class).getId();

                consumer.accept(id);
            }
        });
    }

    public void forEachAdminId(Consumer<String> consumer) {
        db.collection("users")
                .whereEqualTo("type", UserType.ADMIN)
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
                    for (DocumentSnapshot document : queryDocumentSnapshots) {
                        String id = document.toObject(User.class).getId();

                        consumer.accept(id);
                    }
                });
    }

    public void get(String id, Consumer<User> consumer) {
        db.collection("users")
            .document(id)
            .get().addOnSuccessListener(documentSnapshot -> {
                User user = documentSnapshot.toObject(User.class);

                consumer.accept(user);
            });
    }

    public Task<Void> SaveUser(User user){
        return db.collection("users").document().set(user);
    }
}
