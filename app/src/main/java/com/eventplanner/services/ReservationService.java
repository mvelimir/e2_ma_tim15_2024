package com.eventplanner.services;

import android.util.Log;

import androidx.annotation.NonNull;

import com.eventplanner.model.CustomerService;
import com.eventplanner.model.Employee;
import com.eventplanner.model.EmployeeEvent;
import com.eventplanner.model.EmployeeEventCategory;
import com.eventplanner.model.EventPackage;
import com.eventplanner.model.Notification;
import com.eventplanner.model.Owner;
import com.eventplanner.model.Reservation;
import com.eventplanner.model.ReservationStatus;
import com.eventplanner.model.User;
import com.eventplanner.model.UserType;
import com.eventplanner.model.event.Event;
import com.eventplanner.repositories.NotificationRepository;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.function.Consumer;

public class ReservationService {
    private FirebaseFirestore db;
    private UserService userService;

    public ReservationService() {
        db = FirebaseFirestore.getInstance();
        userService = new UserService();
    }

    public void get(String id, Consumer<Reservation> consumer) {
        db.collection("reservations").document(id).get().addOnSuccessListener(documentSnapshot -> {
            Reservation reservation = documentSnapshot.toObject(Reservation.class);

            consumer.accept(reservation);
        });
    }

    public void getAll(Consumer<ArrayList<Reservation>> consumer) {
        userService.getUser(user -> {
            switch (user.getType()) {
                case EMPLOYEE:
                    db.collection("reservations").whereEqualTo("employeeId", user.getId()).get().addOnSuccessListener(queryDocumentSnapshots -> {
                        ArrayList<Reservation> reservations = extractReservations(queryDocumentSnapshots);

                        consumer.accept(reservations);
                    });
                    break;
                case OWNER:
                    userService.get(user.getId(), Owner.class, o -> {
                        db.collection("reservations").whereEqualTo("companyId", o.getCompanyId()).get().addOnSuccessListener(queryDocumentSnapshots -> {
                            ArrayList<Reservation> reservations = extractReservations(queryDocumentSnapshots);

                            consumer.accept(reservations);
                        });
                    });
                    break;
                case ADMIN:
                    db.collection("reservations").get().addOnSuccessListener(queryDocumentSnapshots -> {
                        ArrayList<Reservation> reservations = extractReservations(queryDocumentSnapshots);

                        consumer.accept(reservations);
                    });
                    break;
                default:
                    db.collection("reservations").whereEqualTo("organizerId", user.getId()).get().addOnSuccessListener(queryDocumentSnapshots -> {
                        ArrayList<Reservation> reservations = extractReservations(queryDocumentSnapshots);

                        consumer.accept(reservations);
                    });
                    break;
            }
        });
    }

    public void accept(Reservation reservation, Runnable runnable) {
        DocumentReference reservationRef = db.collection("reservations").document(reservation.getId());

        reservation.setStatus(ReservationStatus.ACCEPTED);
        reservation.setFinishTime(new Date());

        EmployeeEvent employeeEvent = new EmployeeEvent();
        employeeEvent.setDate(reservation.getFrom());
        employeeEvent.setFrom(reservation.getFrom());
        employeeEvent.setTo(reservation.getTo());
        employeeEvent.setCategory(EmployeeEventCategory.RESERVED);

        EmployeeEventService employeeEventService = new EmployeeEventService();

        employeeEventService.create(reservation.getEmployeeId(), employeeEvent, e -> {
            reservation.setEmployeeEventId(e.getId());
            reservationRef.set(reservation);
        });

        runnable.run();

        if (reservation.isPartOfPackage()) {
            db.collection("reservations")
                    .whereEqualTo("packageId", reservation.getPackageId())
                    .get().addOnSuccessListener(queryDocumentSnapshots -> {
                        boolean isReserved = true;

                        for (DocumentSnapshot doc : queryDocumentSnapshots) {
                            if (!doc.getString("id").equals(reservation.getId()) && !doc.getString("status").equals("ACCEPTED") && doc.getDate("cancellationTimeLimit").equals(reservation.getCancellationTimeLimit())) {
                                isReserved = false;
                                break;
                            }
                        }
                        if (isReserved) {
                            //paket je rezervisan
                            DocumentReference packageRef = db.collection("packages").document(reservation.getPackageId());

                            packageRef.collection("products")
                                    .get().addOnSuccessListener(queryDocumentSnapshots1 -> {
                                        for (DocumentSnapshot document : queryDocumentSnapshots1) {
                                            String productId = (String) document.get("productId");

                                            DocumentReference productRef = db.collection("products").document(productId);

                                            productRef.get().addOnSuccessListener(documentSnapshot -> {
                                                String categoryId = (String) documentSnapshot.getString("categoryId");
                                                String subcategoryId = (String) documentSnapshot.getString("subcategoryId");
                                                float price = documentSnapshot.getDouble("price").floatValue();
                                                DocumentReference eventRef = db.collection("events").document(reservation.getEventId());

                                                eventRef.get().addOnSuccessListener(documentSnapshot1 -> {
                                                    Event event = documentSnapshot1.toObject(Event.class);

                                                    event.getBudget().addCostToSubcategory(categoryId, subcategoryId, null, productId, price);

                                                    packageRef.collection("services")
                                                            .get().addOnSuccessListener(queryDocumentSnapshots2 -> {
                                                                for (DocumentSnapshot document1 : queryDocumentSnapshots2) {
                                                                    String serviceId = (String) document1.get("serviceId");

                                                                    DocumentReference serviceRef = db.collection("services").document(serviceId);

                                                                    serviceRef.get().addOnSuccessListener(documentSnapshot2 -> {
                                                                        String categoryId1 = (String) documentSnapshot2.getString("categoryId");
                                                                        String subcategoryId1 = (String) documentSnapshot2.getString("subcategoryId");
                                                                        float price1 = Float.parseFloat(documentSnapshot2.getString("fullPrice"));

                                                                        event.getBudget().addCostToSubcategory(categoryId1, subcategoryId1, serviceId, null, price1);
                                                                        eventRef.set(event);
                                                                    });
                                                                }
                                                            });
                                                });

                                            });
                                        }
                                    });
                        }
                    });
        }
    }

    public void cancel(Reservation reservation, UserType userType, boolean cancelFromPackage, Runnable runnable) {
        DocumentReference reservationRef = db.collection("reservations").document(reservation.getId());

        if (reservation.getStatus() == ReservationStatus.ACCEPTED) {
            db.collection("events").document(reservation.getEmployeeEventId()).delete();
        }

        String notificationMessage = "", userId = "";

        switch (userType) {
            case ORGANIZER:
                reservation.setStatus(ReservationStatus.CANCELLED_BY_ORGANIZER);
                notificationMessage = "An organizer has cancelled their reservation";
                userId = reservation.getEmployeeId();
                break;
            case ADMIN:
                reservation.setStatus(ReservationStatus.CANCELLED_BY_ADMIN);
                notificationMessage = "A reservation you requested has been cancelled by an admin";
                userId = reservation.getOrganizerId();
                break;
            case OWNER:
            case EMPLOYEE:
                reservation.setStatus(ReservationStatus.CANCELLED_BY_EMPLOYEE);
                notificationMessage = "A reservation you requested has been cancelled by an employee";
                userId = reservation.getOrganizerId();
                break;
        }

        reservation.setFinishTime(new Date());
        if (reservation.getEmployeeEventId() != null) {
            db.collection("users").document(reservation.getEmployeeId()).collection("events").document(reservation.getEmployeeEventId()).delete();
            reservation.setEmployeeEventId(null);
        }
        reservationRef.set(reservation);

        Notification n = new Notification();
        n.setUnread(true);
        n.setDate(new Date());
        n.setMessage(notificationMessage);
        n.setUserId(userId);

        if (cancelFromPackage && reservation.isPartOfPackage()) {
            n.setTitle("Cancellation of package reservation");

            NotificationRepository.create(n);

            Notification N = new Notification();

            N.setTitle("Review permission for company");
            N.setMessage("After getting a reservation cancelled you have 5 days to rate the company");
            N.setDate(new Date());
            N.setUnread(true);
            N.setUserId(reservation.getOrganizerId());

            NotificationRepository.create(N);
        } else if (!reservation.isPartOfPackage()) {
            n.setTitle("Cancellation of event reservation");
            NotificationRepository.create(n);

            Notification N = new Notification();

            N.setTitle("Review permission for company");
            N.setMessage("After getting a reservation cancelled you have 5 days to rate the company");
            N.setDate(new Date());
            N.setUnread(true);
            N.setUserId(reservation.getOrganizerId());

            NotificationRepository.create(N);
        }

        if (cancelFromPackage && reservation.isPartOfPackage()) {
            db.collection("reservations")
                    .whereEqualTo("packageId", reservation.getPackageId())
                    .whereEqualTo("cancellationTimeLimit", reservation.getCancellationTimeLimit())
                    .get().addOnSuccessListener(queryDocumentSnapshots -> {
                        for (DocumentSnapshot document : queryDocumentSnapshots) {
                            Reservation reservation1 = document.toObject(Reservation.class);

                            cancel(reservation1, userType, false, runnable);
                        }
                    });
        }

        runnable.run();
    }

    public void getByOrganizerFullName(String organizerFullName, Consumer<ArrayList<Reservation>> consumer) {
        String [] fullNameSplit = splitFullName(organizerFullName);
        String firstName = fullNameSplit[0];
        String lastName = fullNameSplit[1];

        db.collection("users")
            .whereEqualTo("type", UserType.ORGANIZER)
            .whereEqualTo("firstName", firstName)
            .whereEqualTo("lastName", lastName)
            .get().addOnSuccessListener(queryDocumentSnapshots -> {
                ArrayList<String> organizerIds = new ArrayList<>();

                for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                    User organizer = document.toObject(User.class);
                    organizerIds.add(organizer.getId());
                }

                if (organizerIds.isEmpty()) {
                    consumer.accept(new ArrayList<>());
                    return;
                }

                db.collection("reservations")
                    .whereIn("organizerId", organizerIds)
                    .get().addOnSuccessListener(queryDocumentSnapshots1 -> {
                        ArrayList<Reservation> reservations = extractReservations(queryDocumentSnapshots1);

                        consumer.accept(reservations);
                    });
            });
    }

    public void getByEmployeeFullName(String employeeFullName, Consumer<ArrayList<Reservation>> consumer) {
        String [] fullNameSplit = splitFullName(employeeFullName);
        String firstName = fullNameSplit[0];
        String lastName = fullNameSplit[1];

        db.collection("users")
                .whereEqualTo("type", UserType.EMPLOYEE)
                .whereEqualTo("firstName", firstName)
                .whereEqualTo("lastName", lastName)
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<String> employeeIds = new ArrayList<>();

                    for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                        User employee = document.toObject(User.class);
                        employeeIds.add(employee.getId());
                    }

                    if (employeeIds.isEmpty()) {
                        consumer.accept(new ArrayList<>());
                        return;
                    }

                    db.collection("reservations")
                            .whereIn("employeeId", employeeIds)
                            .get().addOnSuccessListener(queryDocumentSnapshots1 -> {
                                ArrayList<Reservation> reservations = extractReservations(queryDocumentSnapshots1);

                                consumer.accept(reservations);
                            });
                });
    }

    public void getByServiceName(String serviceName, Consumer<ArrayList<Reservation>> consumer) {
        db.collection("services")
                .whereEqualTo("name", serviceName)
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<String> serviceIds = new ArrayList<>();

                    for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                        CustomerService srv = document.toObject(CustomerService.class);
                        serviceIds.add(srv.getId());
                    }

                    if (serviceIds.isEmpty()) {
                        consumer.accept(new ArrayList<>());
                        return;
                    }

                    db.collection("reservations")
                            .whereIn("serviceId", serviceIds)
                            .get().addOnSuccessListener(queryDocumentSnapshots1 -> {
                                ArrayList<Reservation> reservations = extractReservations(queryDocumentSnapshots1);

                                consumer.accept(reservations);
                            });
                });
    }

    private String[] splitFullName(String fullName) {
        return fullName.split(" ");
    }

    private ArrayList<Reservation> extractReservations(QuerySnapshot queryDocumentSnapshots) {
        ArrayList<Reservation> reservations = new ArrayList<>();

        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
            Reservation reservation = document.toObject(Reservation.class);

            if (reservation.getStatus() == ReservationStatus.ACCEPTED && reservation.getTo().before(new Date())) {
                reservation.setStatus(ReservationStatus.REALIZED);
                document.getReference().set(reservation);

                Notification n = new Notification();

                n.setTitle("Review permission of company");
                n.setMessage("After realizing a reservation you have 5 days to rate the company");
                n.setDate(new Date());
                n.setUnread(true);
                n.setUserId(reservation.getOrganizerId());

                NotificationRepository.create(n);
            }

            reservations.add(reservation);
        }

        return reservations;
    }
}
