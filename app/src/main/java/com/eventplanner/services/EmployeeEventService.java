package com.eventplanner.services;

import com.eventplanner.model.EmployeeEvent;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.function.Consumer;

public class EmployeeEventService {
    private FirebaseFirestore db;

    public EmployeeEventService() {
        db = FirebaseFirestore.getInstance();
    }

    public void get(String employeeId, String id, Consumer<EmployeeEvent> consumer) {
        db.collection("users")
                .document(employeeId)
                .collection("events")
                .document(id)
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
                    EmployeeEvent event = queryDocumentSnapshots.toObject(EmployeeEvent.class);

                    consumer.accept(event);
                });
    }

    public void create(String employeeId, EmployeeEvent event, Consumer<EmployeeEvent> consumer) {
        DocumentReference eventRef = db.collection("users").document(employeeId).collection("events").document();

        event.setId(eventRef.getId());

        eventRef.set(event);

        consumer.accept(event);
    }
}
