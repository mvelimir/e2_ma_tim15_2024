package com.eventplanner.services;

import com.eventplanner.model.Company;
import com.eventplanner.model.CompanyReview;
import com.eventplanner.model.Reservation;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.function.Consumer;

public class CompanyReviewService {
    private FirebaseFirestore db;

    public CompanyReviewService() {
        db = FirebaseFirestore.getInstance();
    }

    public void create(CompanyReview review) {
        DocumentReference reviewRef = db.collection("company_reviews").document();

        review.setId(reviewRef.getId());
        review.setDate(new Date());
        reviewRef.set(review);
    }

    public void getAllForCompany(String companyId, Consumer<ArrayList<CompanyReview>> consumer) {
        db.collection("company_reviews")
                .whereEqualTo("companyId", companyId)
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<CompanyReview> reviews = new ArrayList<>();

                    for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                        CompanyReview review = document.toObject(CompanyReview.class);
                        reviews.add(review);
                    }

                    consumer.accept(reviews);
                });
    }

    public void delete(String reviewId) {
        db.collection("company_reviews").document(reviewId).delete();
    }

}
