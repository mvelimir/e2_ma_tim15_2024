package com.eventplanner.services;

import com.eventplanner.model.CustomerService;
import com.eventplanner.model.EmployeeEvent;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.function.Consumer;

public class CustomerServiceService {
    private FirebaseFirestore db;

    public CustomerServiceService() {
        db = FirebaseFirestore.getInstance();
    }

    public void get(String id, Consumer<CustomerService> consumer) {
        db.collection("services")
                .document(id)
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
                    CustomerService service = queryDocumentSnapshots.toObject(CustomerService.class);

                    consumer.accept(service);
                });
    }
}
