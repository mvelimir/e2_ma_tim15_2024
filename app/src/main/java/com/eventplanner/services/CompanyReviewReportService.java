package com.eventplanner.services;

import android.util.Log;

import com.eventplanner.model.CompanyReviewReport;
import com.eventplanner.model.CompanyReviewReportStatus;
import com.eventplanner.repositories.NotificationRepository;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Date;
import java.util.function.Consumer;

public class CompanyReviewReportService {
    private FirebaseFirestore db;

    public CompanyReviewReportService() { db = FirebaseFirestore.getInstance(); }

    public void create(CompanyReviewReport report) {
        DocumentReference reportRef = db.collection("company_review_reports").document();

        report.setId(reportRef.getId());
        report.setDate(new Date());
        report.setStatus(CompanyReviewReportStatus.REPORTED);
        reportRef.set(report);
    }

    public void getAll(Consumer<ArrayList<CompanyReviewReport>> consumer) {
        db.collection("company_review_reports")
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<CompanyReviewReport> reports = new ArrayList<>();

                    for (DocumentSnapshot document : queryDocumentSnapshots) {
                        CompanyReviewReport report = document.toObject(CompanyReviewReport.class);
                        reports.add(report);
                    }

                    consumer.accept(reports);
                });
    }

    public void update(CompanyReviewReport report) {
        DocumentReference reportRef = db.collection("company_review_reports").document(report.getId());

        reportRef.set(report);
    }

    public void getByReviewId(String reviewId, Consumer<CompanyReviewReport> consumer) {
        db.collection("company_review_reports")
                .whereEqualTo("reviewId", reviewId)
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
                    CompanyReviewReport report = null;

                    try {
                        report = queryDocumentSnapshots.getDocuments().get(0).toObject(CompanyReviewReport.class);
                    } catch(Exception ignored) {
                    }

                    consumer.accept(report);
                });
    }
}
