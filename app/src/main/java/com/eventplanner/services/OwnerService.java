package com.eventplanner.services;

import androidx.annotation.NonNull;

import com.eventplanner.model.Owner;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class OwnerService {

    private FirebaseFirestore db;

    public OwnerService() {
        db = FirebaseFirestore.getInstance();
    }

    public void getAll(OnOwnersRetrievedListener listener) {
        db.collection("owners")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Owner> ownerList = new ArrayList<>();
                            for (DocumentSnapshot document : task.getResult()) {
                                Owner owner = document.toObject(Owner.class);
                                ownerList.add(owner);
                            }
                            listener.onOwnersRetrieved(ownerList);
                        } else {
                            listener.onError(task.getException());
                        }
                    }
                });
    }

    public interface OnOwnersRetrievedListener {
        void onOwnersRetrieved(List<Owner> ownerList);

        void onError(Exception e);
    }
}
