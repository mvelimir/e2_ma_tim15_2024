package com.eventplanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.eventplanner.model.CompanyReview;
import com.eventplanner.model.Notification;
import com.eventplanner.model.Reservation;
import com.eventplanner.model.ReservationStatus;
import com.eventplanner.services.CompanyReviewService;
import com.eventplanner.model.User;
import com.eventplanner.model.UserType;
import com.eventplanner.repositories.NotificationRepository;
import com.eventplanner.services.ReservationService;
import com.eventplanner.services.UserService;
import com.eventplanner.ui.Owner.RegisterOwnerFragment;
import com.eventplanner.ui.companies.CompanyFragment;
import com.eventplanner.ui.employees.EmployeeEventFormFragment;
import com.eventplanner.ui.employees.EmployeeFragment;
import com.eventplanner.ui.employees.EmployeeRegistrationFragment;
import com.eventplanner.ui.employees.EmployeeScheduleFormFragment;
import com.eventplanner.ui.employees.EmployeesFragment;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import android.Manifest;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.eventplanner.databinding.ActivityMainBinding;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;

    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    // FCM SDK (and your app) can post notifications.
                } else {
                    // TODO: Inform user that that your app will not show notifications.
                }
            });

    private void askNotificationPermission() {
        // This is only necessary for API level >= 33 (TIRAMISU)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) ==
                    PackageManager.PERMISSION_GRANTED) {
                // FCM SDK (and your app) can post notifications.
            } else if (shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS)) {
            } else {
                // Directly ask for the permission
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.navView.inflateMenu(R.menu.logged_out_drawer);

        if (Build.VERSION.SDK_INT >= 33) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.POST_NOTIFICATIONS},101);
            }
        }

        setNavView();

        setSupportActionBar(binding.appBarMain.toolbar);

        String notificationListenerString = Settings.Secure.getString(this.getContentResolver(), "enabled_notification_listeners");
        if (notificationListenerString == null || !notificationListenerString.contains(getPackageName())) {
            Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            startActivity(intent);
        }

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);

        Activity a = this;

        binding.appBarMain.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserService userService = new UserService();

                Fragment navHostFragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_content_main);
                Fragment displayedFragment = navHostFragment == null ? null : navHostFragment.getChildFragmentManager().getFragments().get(0);

                if (displayedFragment instanceof EmployeesFragment) {
                    EmployeesFragment fragment = (EmployeesFragment) displayedFragment;

                    Bundle bundle = new Bundle();
                    bundle.putString("companyId", fragment.getCompanyId());
                    navController.navigate(R.id.nav_employee_registration, bundle);
                } else if (displayedFragment instanceof EmployeeRegistrationFragment) {
                    EmployeeRegistrationFragment fragment = (EmployeeRegistrationFragment) displayedFragment;
                    if (fragment.register()) {
                        navController.navigate(R.id.nav_employees);
                    } else {
                        Toast.makeText(a, "Invalid data", Toast.LENGTH_SHORT).show();
                    }
                } else if (displayedFragment instanceof EmployeeFragment) {
                    EmployeeFragment fragment = (EmployeeFragment) displayedFragment;

                    if (fragment.tabPosition == 0) {
                        Bundle bundle = new Bundle();
                        bundle.putString("employeeId", fragment.getEmployeeId());

                        navController.navigate(R.id.nav_employee_event_form, bundle);
                    } else if (fragment.tabPosition == 1) {
                        userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
                            User user = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class);

                            if (user.getType() == UserType.OWNER) {
                                Bundle bundle = new Bundle();
                                bundle.putString("employeeId", fragment.getEmployeeId());
                                navController.navigate(R.id.nav_schedule_form, bundle);
                            }
                        });
                    }
                } else if (displayedFragment instanceof EmployeeEventFormFragment) {
                    EmployeeEventFormFragment fragment = (EmployeeEventFormFragment) displayedFragment;

                    if (fragment.register()) {
                        userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
                            User user = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class);

                            Notification notification = new Notification();
                            if (user.getType() == UserType.OWNER) {
                                notification.setUserId(fragment.getEmployeeId());
                                notification.setTitle("Event reservation");
                                notification.setMessage("The owner reserved an event for you.");
                            } else {
                                notification.setUserId(fragment.getOwnerId());
                                notification.setTitle("Event added");
                                notification.setMessage("An employee added an event.");
                            }

                            NotificationRepository.create(notification);

                            Toast.makeText(a, "Notification sent", Toast.LENGTH_SHORT).show();

                            Bundle bundle = new Bundle();
                            bundle.putString("employeeId", fragment.getEmployeeId());

                            navController.navigate(R.id.nav_employee, bundle);
                        });
                    } else {
                        Toast.makeText(a, "Invalid data", Toast.LENGTH_SHORT).show();
                    }
                } else if (displayedFragment instanceof EmployeeScheduleFormFragment) {
                    EmployeeScheduleFormFragment fragment = (EmployeeScheduleFormFragment) displayedFragment;

                    if (fragment.register()) {
                        Bundle bundle = new Bundle();
                        bundle.putString("employeeId", fragment.getEmployeeId());

                        navController.navigate(R.id.nav_employee, bundle);
                    } else {
                        Toast.makeText(a, "Invalid data", Toast.LENGTH_SHORT).show();
                    }
                } else if (displayedFragment instanceof CompanyFragment) {
                    CompanyFragment fragment = (CompanyFragment) displayedFragment;

                    ReservationService service = new ReservationService();
                    CompanyReviewService reviewService = new CompanyReviewService();

                    userService.getUser(u -> {
                        if (u.getType() == UserType.ORGANIZER || u.getType() == null) {
                            reviewService.getAllForCompany(fragment.getCompanyId(), crs -> {
                                for (CompanyReview r : crs) {
                                    if (r.getOrganizerId().equals(u.getId())) return;
                                }

                                service.getByOrganizerFullName(u.getFirstName() + " " + u.getLastName(), rs -> {
                                    for (Reservation r : rs) {
                                        if (r.getStatus() != ReservationStatus.ACCEPTED
                                                && r.getStatus() != ReservationStatus.NEW
                                                && isLessThanFiveDaysAgo(r.getFinishTime())) {
                                            showReviewDialog(fragment.getCompanyId());
                                        }
                                    }
                                });
                            });
                        }
                    });
                } else if(displayedFragment instanceof RegisterOwnerFragment){
                    RegisterOwnerFragment fragment = (RegisterOwnerFragment) displayedFragment;

                        Bundle bundle = new Bundle();
                        bundle.putString("ownerId", fragment.getOwnerId());

                        navController.navigate(R.id.registerCompanyFragment, bundle);
                }

                else {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null)
                            .setAnchorView(R.id.fab).show();
                }
            }
        });

        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_products_owner, R.id.nav_products_employee, R.id.nav_service_owner, R.id.nav_employee,
                R.id.employeeServiceListFragment, R.id.ownerEventPackageListFragment, R.id.employeeEventPackageListFragment)
                .setOpenableLayout(drawer)
                .build();
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    public void setNavView() {
        UserService userService = new UserService();

        if (userService.isLoggedIn()) {
            userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
                User user = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class);

                switch (user.getType()) {
                    case EMPLOYEE:
                        binding.navView.getMenu().clear();
                        binding.navView.inflateMenu(R.menu.employee_drawer);
                        break;
                    case OWNER:
                        binding.navView.getMenu().clear();
                        binding.navView.inflateMenu(R.menu.owner_drawer);
                        break;
                    case ADMIN:
                        binding.navView.getMenu().clear();
                        binding.navView.inflateMenu(R.menu.admin_drawer);
                        break;
                    default:
                        binding.navView.getMenu().clear();
                        binding.navView.inflateMenu(R.menu.organizer_drawer);
                        break;
                }
            });
        } else {
            binding.navView.getMenu().clear();
            binding.navView.inflateMenu(R.menu.logged_out_drawer);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private static boolean isLessThanFiveDaysAgo(Date finishTime) {
        Calendar now = Calendar.getInstance();

        now.add(Calendar.DAY_OF_YEAR, -5);

        Date fiveDaysAgo = now.getTime();

        return !finishTime.before(fiveDaysAgo);
    }

    private void showReviewDialog(String companyId) {
        LayoutInflater inflater = LayoutInflater.from(MainActivity.this);

        View dialogView = inflater.inflate(R.layout.dialog_company_review, null);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);

        dialogBuilder.setView(dialogView);

        EditText commentField = dialogView.findViewById(R.id.comment_field);
        Spinner ratingOption = dialogView.findViewById(R.id.rating_option);

        dialogBuilder.setPositiveButton("Confirm", (dialog, which) -> {
            int rating = Integer.parseInt((String) ratingOption.getSelectedItem());
            String comment = commentField.getText().toString();

            if (comment.isEmpty()) return;

            UserService userService = new UserService();

            userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
                String organizerId = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class).getId();

                CompanyReview review = new CompanyReview();
                review.setRating(rating);
                review.setComment(comment);
                review.setCompanyId(companyId);
                review.setOrganizerId(organizerId);

                CompanyReviewService service = new CompanyReviewService();

                service.create(review);

                userService.getOwnerIdFromCompanyId(companyId, ownerId -> {
                    Notification n = new Notification();
                    n.setMessage("Your company got a new " + Integer.valueOf(rating).toString() + " star review");
                    n.setTitle("New review of company");
                    n.setUserId(ownerId);
                    n.setDate(new Date());

                    NotificationRepository.create(n);
                });
            });
        });

        dialogBuilder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }
}