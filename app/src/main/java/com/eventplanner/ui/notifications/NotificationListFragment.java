package com.eventplanner.ui.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.eventplanner.adapters.NotificationListAdapter;
import com.eventplanner.adapters.ReservationListAdapter;
import com.eventplanner.databinding.FragmentNotificationListBinding;
import com.eventplanner.databinding.FragmentReservationListBinding;
import com.eventplanner.model.Notification;
import com.eventplanner.model.Reservation;
import com.eventplanner.ui.reservations.ReservationListFragment;

import java.util.ArrayList;

public class NotificationListFragment extends ListFragment {
    private NotificationListAdapter adapter;
    private ArrayList<Notification> notifications;
    private FragmentNotificationListBinding binding;

    public static NotificationListFragment newInstance(ArrayList<Notification> notifications){
        NotificationListFragment fragment = new NotificationListFragment();
        fragment.notifications = notifications;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new NotificationListAdapter(getActivity(), notifications);
        setListAdapter(adapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentNotificationListBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
