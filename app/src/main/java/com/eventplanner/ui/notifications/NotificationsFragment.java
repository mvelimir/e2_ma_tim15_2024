package com.eventplanner.ui.notifications;

import static android.content.Context.SENSOR_SERVICE;

import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.hardware.Sensor;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentNotificationsBinding;
import com.eventplanner.model.Notification;
import com.eventplanner.model.User;
import com.eventplanner.repositories.NotificationRepository;
import com.eventplanner.services.UserService;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class NotificationsFragment extends Fragment implements SensorEventListener {
    public static ArrayList<Notification> notifications;
    public static ArrayList<Notification> filteredNotifications;
    private FragmentNotificationsBinding binding;
    private Spinner filterOption;

    private String[] filterStates = { "All", "Unread", "Read" };
    private int filterPosition;

    private UserService userService;

    private static final float SHAKE_THRESHOLD = 3.25f; // m/S**2
    private static final int MIN_TIME_BETWEEN_SHAKES_MILLISECS = 1000;
    private long mLastShakeTime;
    private SensorManager mSensorMgr;


    public NotificationsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        notifications = new ArrayList<>();
        filteredNotifications = new ArrayList<>();

        filterOption = binding.filterOption;

        filterPosition = 0;

        userService = new UserService();

        filterOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                filterNotifications();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
            User user = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class);

            NotificationRepository.getAllNotificationsForUser(user.getId(), ns -> {
                notifications = ns;
                filterNotifications();
            });
        });

        mSensorMgr = (SensorManager)getActivity().getSystemService(SENSOR_SERVICE);

        // Listen for shakes
        Sensor accelerometer = mSensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null) {
            mSensorMgr.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }

        return root;
    }

    private void filterNotifications() {
        switch (filterOption.getSelectedItem().toString()) {
            case "All":
                filteredNotifications = notifications;
                break;
            case "Unread":
                filteredNotifications = notifications.stream().filter(n -> n.isUnread()).collect(Collectors.toCollection(ArrayList::new));
                break;
            case "Read":
                filteredNotifications = notifications.stream().filter(n -> !n.isUnread()).collect(Collectors.toCollection(ArrayList::new));
                break;
        }

        FragmentTransition.to(NotificationListFragment.newInstance(filteredNotifications), getActivity(),
                false, R.id.scroll_notifications_list);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            long curTime = System.currentTimeMillis();
            if ((curTime - mLastShakeTime) > MIN_TIME_BETWEEN_SHAKES_MILLISECS) {

                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];

                double acceleration = Math.sqrt(Math.pow(x, 2) +
                        Math.pow(y, 2) +
                        Math.pow(z, 2)) - SensorManager.GRAVITY_EARTH;

                if (acceleration > SHAKE_THRESHOLD) {
                    mLastShakeTime = curTime;
                    filterPosition = (filterPosition + 1) % filterStates.length;
                    filterOption.setSelection(filterPosition);
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}