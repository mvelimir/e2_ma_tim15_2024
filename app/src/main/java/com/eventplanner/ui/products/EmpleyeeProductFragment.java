package com.eventplanner.ui.products;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmpleyeeProductBinding;
import com.eventplanner.model.Product;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;

public class EmpleyeeProductFragment extends Fragment {

    private FragmentEmpleyeeProductBinding binding;
    public static ArrayList<Product> products = new ArrayList<Product>();
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.FullScreenBottomSheetDialog);
                View dialogView = getLayoutInflater().inflate(R.layout.bottom_sheet_filter, null);
                bottomSheetDialog.setContentView(dialogView);
                bottomSheetDialog.show();
                //Navigation.findNavController(view).navigate(R.id.action_nav_products_owner_to_nav_filter);
            }
        });
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        createProducts(products);
        binding = FragmentEmpleyeeProductBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        FragmentTransition.to(ProductList.newInstance(products), getActivity(),
                false, R.id.scroll_products_list);
        return view;
    }

    public void createProducts(ArrayList<Product> products) {

        // Creating 5 instances of Product
        /*Product product1 = new Product(1L, "Electronics", "Smartphones", "iPhone 12", "Latest iPhone model", "1000", "10%", "900", "image1", "Tech", true, true);
        Product product2 = new Product(2L, "Clothing", "T-shirts", "Cotton T-shirt", "Comfortable cotton T-shirt", "20", "5%", "19", "image2", "Fashion", true, false);
        Product product3 = new Product(3L, "Books", "Fiction", "Harry Potter", "Magical adventure", "15", "0%", "15", "image3", "Entertainment", true, true);
        Product product4 = new Product(4L, "Home", "Kitchenware", "Coffee Maker", "Brew coffee at home", "50", "15%", "42.5", "image4", "Home & Living", true, true);
        Product product5 = new Product(5L, "Sports", "Fitness", "Yoga Mat", "Premium quality yoga mat", "30", "20%", "24", "image5", "Health & Fitness", true, true);

        // Adding products to the list
        products.add(product1);
        products.add(product2);
        products.add(product3);
        products.add(product4);
        products.add(product5);*/
    }
}