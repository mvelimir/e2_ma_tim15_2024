package com.eventplanner.ui.employees;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Spinner;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeesBinding;
import com.eventplanner.model.Employee;
import com.eventplanner.model.Owner;
import com.eventplanner.model.UserType;
import com.eventplanner.services.UserService;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


import java.util.ArrayList;

public class EmployeesFragment extends Fragment {

    public static ArrayList<Employee> employees = new ArrayList<Employee>();
    private EmployeesViewModel mViewModel;
    private FragmentEmployeesBinding binding;

    private String companyId;

    public String getCompanyId() { return companyId; }

    public void setCompanyId(String companyId) { this.companyId = companyId; }

    private SearchView search;
    private Spinner searchOption;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public static EmployeesFragment newInstance() {
        return new EmployeesFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentEmployeesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        search = binding.search;
        searchOption = binding.searchOption;

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.isEmpty()) {
                    getAll();

                    return true;
                }

                switch (searchOption.getSelectedItem().toString()) {
                    case "First name":
                        search("firstName", query);
                        break;
                    case "Last name":
                        search("lastName", query);
                        break;
                    case "Full name":
                        String[] name = query.split(" ");
                        if (name.length == 2) {
                            search2("firstName", name[0], "lastName", name[1]);
                        }
                        break;
                    case "Email":
                        search("email", query);
                        break;
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.isEmpty()){
                    this.onQueryTextSubmit("");
                }

                return true;
            }
        });

        UserService userService = new UserService();
        userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
            Owner owner = queryDocumentSnapshots.getDocuments().get(0).toObject(Owner.class);
            companyId = owner.getCompanyId();

            getAll();
        });

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(EmployeesViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void search(String by, String query) {
        db
            .collection("users")
            .whereEqualTo("companyId", companyId)
            .whereEqualTo(by, query)
            .get()
            .addOnCompleteListener(this::populateList);
    }

    private void search2(String by1, String query1, String by2, String query2) {
        db
                .collection("users")
                .whereEqualTo("companyId", companyId)
                .whereEqualTo(by1, query1)
                .whereEqualTo(by2, query2)
                .get()
                .addOnCompleteListener(this::populateList);
    }

    private void getAll() {
        db.collection("users")
            .whereEqualTo("companyId", companyId)
            .get()
            .addOnCompleteListener(this::populateList);
    }

    private void populateList(Task<QuerySnapshot> task) {
        if (task.isSuccessful()) {
            employees.clear();

            for (QueryDocumentSnapshot document : task.getResult()) {
                Employee employee = document.toObject(Employee.class);
                if (employee.getType() == UserType.EMPLOYEE) {
                    employees.add(employee);
                }
            }

            FragmentTransition.to(EmployeeListFragment.newInstance(employees), getActivity(),
                    false, R.id.scroll_employees_list);
        } else {
            Log.w("REZ_DB", "Error getting documents.", task.getException());
        }
    }

    public void Test(View view) {
        Snackbar.make(view, "Works I guess", Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .setAnchorView(R.id.fab).show();
    }

}