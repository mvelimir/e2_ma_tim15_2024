package com.eventplanner.ui.employees;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeeEventListBinding;
import com.eventplanner.databinding.FragmentEmployeeListBinding;
import com.eventplanner.model.Employee;
import com.eventplanner.model.EmployeeEvent;

import java.util.ArrayList;

public class EmployeeEventListFragment extends ListFragment {
    private EmployeeEventListAdapter adapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<EmployeeEvent> mEvents;
    private FragmentEmployeeEventListBinding binding;

    public EmployeeEventListFragment(ArrayList<EmployeeEvent> events) {
        this.mEvents = events;
    }

    public static EmployeeEventListFragment newInstance(ArrayList<EmployeeEvent> events) {
        return new EmployeeEventListFragment(events);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new EmployeeEventListAdapter(getActivity(), mEvents);
        setListAdapter(adapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEmployeeEventListBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}