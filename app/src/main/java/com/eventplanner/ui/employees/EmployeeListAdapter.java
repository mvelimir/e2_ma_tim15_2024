package com.eventplanner.ui.employees;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.util.BuddhistCalendar;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.model.Employee;
import com.eventplanner.services.UserService;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class EmployeeListAdapter extends ArrayAdapter<Employee> implements View.OnClickListener {
    StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    private FragmentActivity activity;
    private NavController navController;

    public EmployeeListAdapter(FragmentActivity activity, ArrayList<Employee> employees) {
        super(activity, 0, employees);
        this.activity = activity;
        navController = Navigation.findNavController(this.activity, R.id.nav_host_fragment_content_main);
    }

    public FirebaseFirestore db = FirebaseFirestore.getInstance();

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Employee employee = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.employee_card, parent, false);
        }

        Bundle bundle = new Bundle();
        bundle.putString("employeeId", employee.getId());

        convertView.setOnClickListener(view -> navController.navigate(R.id.nav_employee, bundle));

        CircleImageView profileImageView = convertView.findViewById(R.id.profile_image);
        TextView fullName = convertView.findViewById(R.id.full_name);
        TextView email = convertView.findViewById(R.id.email);
        MaterialButton blockButton = convertView.findViewById(R.id.block_button);

        if (!employee.getImage().isEmpty()) {
            StorageReference profilePictureReference = storageReference.child(employee.getImage());

            final long TEN_MEGABYTES = 10 * 1024 * 1024;
            profilePictureReference.getBytes(TEN_MEGABYTES).addOnSuccessListener(bytes -> {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                profileImageView.setImageBitmap(bmp);
            }).addOnFailureListener(exception -> Toast.makeText(getContext(), "No Such file or Path found!!", Toast.LENGTH_LONG).show());
        }

        fullName.setText(employee.getFirstName() + " " + employee.getLastName());
        email.setText(employee.getEmail());
        blockButton.setText(employee.isBlocked() ? "Unblock" : "Block");

        blockButton.setOnClickListener(view -> {
            employee.setBlocked(!employee.isBlocked());
            blockButton.setText(employee.isBlocked() ? "Unblock" : "Block");

            FirebaseAuth auth = FirebaseAuth.getInstance();

            if (employee.isBlocked()) {
                auth.signInWithEmailAndPassword(employee.getEmail(), employee.getPassword()).addOnSuccessListener(unused -> {
                    auth.getCurrentUser().delete();

                    auth.signInWithEmailAndPassword(UserService.email, UserService.password);
                });
            } else {
                auth.createUserWithEmailAndPassword(employee.getEmail(), employee.getPassword()).addOnSuccessListener(unused -> {
                    auth.getCurrentUser().sendEmailVerification();

                    auth.signInWithEmailAndPassword(UserService.email, UserService.password);
                });
            }

            db
                .collection("users")
                .document(employee.getId())
                .update("blocked", employee.isBlocked())
                .addOnSuccessListener(unused -> Toast.makeText(getContext(), employee.getFirstName() + " " + employee.getLastName() + " " + (employee.isBlocked() ? "blocked" : "unblocked"), Toast.LENGTH_LONG).show());
        });

        return convertView;
    }

    @Override
    public void onClick(View v) {

    }
}
