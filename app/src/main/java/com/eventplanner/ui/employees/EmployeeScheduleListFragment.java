package com.eventplanner.ui.employees;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.eventplanner.databinding.FragmentEmployeeScheduleListBinding;
import com.eventplanner.model.EmployeeSchedule;

import java.util.ArrayList;

public class EmployeeScheduleListFragment extends ListFragment {
    private static final String ARG_PARAM = "param";

    private EmployeeScheduleListAdapter adapter;
    private ArrayList<EmployeeSchedule> schedules;
    private FragmentEmployeeScheduleListBinding binding;


    public EmployeeScheduleListFragment(ArrayList<EmployeeSchedule> schedules) {
        this.schedules = schedules;
    }

    public static EmployeeScheduleListFragment newInstance(ArrayList<EmployeeSchedule> schedules) {

        return new EmployeeScheduleListFragment(schedules);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new EmployeeScheduleListAdapter(getActivity(), schedules);
        setListAdapter(adapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEmployeeScheduleListBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
