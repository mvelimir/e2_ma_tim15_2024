package com.eventplanner.ui.employees;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.eventplanner.R;
import com.eventplanner.model.EmployeeEvent;
import com.eventplanner.model.EmployeeSchedule;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class EmployeeScheduleListAdapter extends ArrayAdapter<EmployeeSchedule> {
    public EmployeeScheduleListAdapter(Context context, ArrayList<EmployeeSchedule> schedules) {
        super(context, 0, schedules);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        EmployeeSchedule schedule = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.employee_schedule_card, parent, false);
        }

        TextView dateRangeText = convertView.findViewById(R.id.dateRangeText);

        TextView mondayStartText = convertView.findViewById(R.id.mondayStartText);
        TextView mondayEndText = convertView.findViewById(R.id.mondayEndText);

        TextView tuesdayStartText = convertView.findViewById(R.id.tuesdayStartText);
        TextView tuesdayEndText = convertView.findViewById(R.id.tuesdayEndText);

        TextView wednesdayStartText = convertView.findViewById(R.id.wednesdayStartText);
        TextView wednesdayEndText = convertView.findViewById(R.id.wednesdayEndText);

        TextView thursdayStartText = convertView.findViewById(R.id.thursdayStartText);
        TextView thursdayEndText = convertView.findViewById(R.id.thursdayEndText);

        TextView fridayStartText = convertView.findViewById(R.id.fridayStartText);
        TextView fridayEndText = convertView.findViewById(R.id.fridayEndText);

        TextView saturdayStartText = convertView.findViewById(R.id.saturdayStartText);
        TextView saturdayEndText = convertView.findViewById(R.id.saturdayEndText);

        TextView sundayStartText = convertView.findViewById(R.id.sundayStartText);
        TextView sundayEndText = convertView.findViewById(R.id.sundayEndText);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        if (schedule.getTo() == null) {
            dateRangeText.setText(dateFormat.format(schedule.getFrom()) + " - " + "          ");
        } else {
            dateRangeText.setText(dateFormat.format(schedule.getFrom()) + " - " + dateFormat.format(schedule.getTo()));
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

        if (schedule.getMondayStart() == null) {
            mondayStartText.setText("Day off");
        } else {
            mondayStartText.setText(timeFormat.format(schedule.getMondayStart()));
            mondayEndText.setText(timeFormat.format(schedule.getMondayEnd()));
        }

        if (schedule.getTuesdayStart() == null) {
            tuesdayStartText.setText("Day off");
        } else {
            tuesdayStartText.setText(timeFormat.format(schedule.getTuesdayStart()));
            tuesdayEndText.setText(timeFormat.format(schedule.getTuesdayEnd()));
        }

        if (schedule.getWednesdayStart() == null) {
            wednesdayStartText.setText("Day off");
        } else {
            wednesdayStartText.setText(timeFormat.format(schedule.getWednesdayStart()));
            wednesdayEndText.setText(timeFormat.format(schedule.getWednesdayEnd()));
        }

        if (schedule.getThursdayStart() == null) {
            thursdayStartText.setText("Day off");
        } else {
            thursdayStartText.setText(timeFormat.format(schedule.getThursdayStart()));
            thursdayEndText.setText(timeFormat.format(schedule.getThursdayEnd()));
        }

        if (schedule.getFridayStart() == null) {
            fridayStartText.setText("Day off");
        } else {
            fridayStartText.setText(timeFormat.format(schedule.getFridayStart()));
            fridayEndText.setText(timeFormat.format(schedule.getFridayEnd()));
        }

        if (schedule.getSaturdayStart() == null) {
            saturdayStartText.setText("Day off");
        } else {
            saturdayStartText.setText(timeFormat.format(schedule.getSaturdayStart()));
            saturdayEndText.setText(timeFormat.format(schedule.getSaturdayEnd()));
        }

        if (schedule.getSundayStart() == null) {
            sundayStartText.setText("Day off");
        } else {
            sundayStartText.setText(timeFormat.format(schedule.getSundayStart()));
            sundayEndText.setText(timeFormat.format(schedule.getSundayEnd()));
        }

        return convertView;
    }
}
