package com.eventplanner.ui.employees;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeeListBinding;
import com.eventplanner.model.Employee;

import java.util.ArrayList;

public class EmployeeListFragment extends ListFragment {
    private EmployeeListAdapter adapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<Employee> mEmployees;
    private FragmentEmployeeListBinding binding;

    public static EmployeeListFragment newInstance(ArrayList<Employee> employees){
        EmployeeListFragment fragment = new EmployeeListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, employees);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mEmployees = getArguments().getParcelableArrayList(ARG_PARAM);
            adapter = new EmployeeListAdapter(getActivity(), mEmployees);
            setListAdapter(adapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEmployeeListBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}