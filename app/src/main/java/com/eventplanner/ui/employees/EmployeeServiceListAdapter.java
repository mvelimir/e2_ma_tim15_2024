package com.eventplanner.ui.employees;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.eventplanner.R;
import com.eventplanner.model.CustomerService;

import java.util.ArrayList;

public class EmployeeServiceListAdapter extends ArrayAdapter<CustomerService> {
    private ArrayList<CustomerService> aServices;

    public EmployeeServiceListAdapter(Context context, ArrayList<CustomerService> services){
        super(context, R.layout.employee_service_card, services);
        aServices = services;

    }

    @Override
    public int getCount() {
        return aServices.size();
    }

    @Nullable
    @Override
    public CustomerService getItem(int position) {
        return aServices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CustomerService service = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.employee_service_card, parent, false);
        }

        TextView nameText = convertView.findViewById(R.id.text_name_service);
        TextView descriptionText = convertView.findViewById(R.id.text_description_service);
        TextView locationText = convertView.findViewById(R.id.text_location_service);
        TextView durationText = convertView.findViewById(R.id.text_duration_service);
        TextView priceText = convertView.findViewById(R.id.text_price_service);

        nameText.setText(service.getName());
        descriptionText.setText(service.getDescription());
        locationText.setText(service.getLocation());
        durationText.setText(service.getDuration());
        priceText.setText(service.getFullPrice());

        return convertView;
    }

}
