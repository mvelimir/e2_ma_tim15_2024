package com.eventplanner.ui.employees;

import static java.lang.Math.abs;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeeScheduleBinding;
import com.eventplanner.databinding.FragmentEmployeeScheduleFormBinding;
import com.eventplanner.databinding.FragmentEmployeeScheduleListBinding;
import com.eventplanner.model.Employee;
import com.eventplanner.model.EmployeeSchedule;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EmployeeScheduleFormFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeScheduleFormFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EditText fromText;
    private EditText toText;
    private EditText mondayStartText;
    private EditText mondayEndText;
    private EditText tuesdayStartText;
    private EditText tuesdayEndText;
    private EditText wednesdayStartText;
    private EditText wednesdayEndText;
    private EditText thursdayStartText;
    private EditText thursdayEndText;
    private EditText fridayStartText;
    private EditText fridayEndText;
    private EditText saturdayStartText;
    private EditText saturdayEndText;
    private EditText sundayStartText;
    private EditText sundayEndText;

    private String employeeId;

    public String getEmployeeId() { return employeeId; }

    private FragmentEmployeeScheduleFormBinding binding;

    public ArrayList<EmployeeSchedule> schedules = new ArrayList<>();

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public EmployeeScheduleFormFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmployeeScheduleFormFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmployeeScheduleFormFragment newInstance(String param1, String param2) {
        EmployeeScheduleFormFragment fragment = new EmployeeScheduleFormFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeScheduleFormBinding.inflate(inflater, container, false);

        Bundle bundle = getArguments();
        employeeId = bundle.getString("employeeId");

        db
            .collection("users")
            .document(employeeId)
            .collection("schedules")
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    schedules.clear();

                    for (QueryDocumentSnapshot document : task.getResult()) {
                        EmployeeSchedule schedule = document.toObject(EmployeeSchedule.class);
                        schedules.add(schedule);
                    }
                } else {
                    Log.w("REZ_DB", "Error getting documents.", task.getException());
                }
            });

        fromText = binding.scheduleStartField;
        toText = binding.scheduleEndField;

        mondayStartText = binding.mondayStartField;
        mondayEndText = binding.mondayEndField;
        tuesdayStartText = binding.tuesdayStartField;
        tuesdayEndText = binding.tuesdayEndField;
        wednesdayStartText = binding.wednesdayStartField;
        wednesdayEndText = binding.wednesdayEndField;
        thursdayStartText = binding.thursdayStartField;
        thursdayEndText = binding.thursdayEndField;
        fridayStartText = binding.fridayStartField;
        fridayEndText = binding.fridayEndField;
        saturdayStartText = binding.saturdayStartField;
        saturdayEndText = binding.saturdayEndField;
        sundayStartText = binding.sundayStartField;
        sundayEndText = binding.sundayEndField;


        return binding.getRoot();
    }

    public boolean register() {
        EmployeeSchedule schedule;

        try {
            Date from = dateStringToDate(fromText.getText().toString());
            Date to = dateStringToDate(toText.getText().toString());

            if (!from.after(localDateToDate(LocalDate.now().minusDays(1)))) return false;
            if (!to.after(from) || !isDifferenceWeekMultiple(from, to)) return false;

            for (EmployeeSchedule s : schedules) {
                if (s.getTo() == null) continue;
                if (!(!to.after(s.getFrom()) || !from.before(s.getTo()))) return false;
            }

            Date mondayStart = null;
            Date mondayEnd = null;
            if (!mondayStartText.getText().toString().isEmpty() || !mondayEndText.getText().toString().isEmpty()) {
                mondayStart = timeStringToDate(mondayStartText.getText().toString());
                mondayEnd = timeStringToDate(mondayEndText.getText().toString());

                if (!mondayEnd.after(mondayStart)) return false;
            }

            Date tuesdayStart = null;
            Date tuesdayEnd = null;
            if (!tuesdayStartText.getText().toString().isEmpty() && !tuesdayEndText.getText().toString().isEmpty()) {
                tuesdayStart = timeStringToDate(tuesdayStartText.getText().toString());
                tuesdayEnd = timeStringToDate(tuesdayEndText.getText().toString());

                if (!tuesdayEnd.after(tuesdayStart)) return false;
            }

            Date wednesdayStart = null;
            Date wednesdayEnd = null;
            if (!wednesdayStartText.getText().toString().isEmpty() && !wednesdayEndText.getText().toString().isEmpty()) {
                wednesdayStart = timeStringToDate(wednesdayStartText.getText().toString());
                wednesdayEnd = timeStringToDate(wednesdayEndText.getText().toString());

                if (!wednesdayEnd.after(wednesdayStart)) return false;
            }

            Date thursdayStart = null;
            Date thursdayEnd = null;
            if (!thursdayStartText.getText().toString().isEmpty() && !thursdayEndText.getText().toString().isEmpty()) {
                thursdayStart = timeStringToDate(thursdayStartText.getText().toString());
                thursdayEnd = timeStringToDate(thursdayEndText.getText().toString());

                if (!thursdayEnd.after(thursdayStart)) return false;
            }

            Date fridayStart = null;
            Date fridayEnd = null;
            if (!fridayStartText.getText().toString().isEmpty() && !fridayEndText.getText().toString().isEmpty()) {
                fridayStart = timeStringToDate(fridayStartText.getText().toString());
                fridayEnd = timeStringToDate(fridayEndText.getText().toString());

                if (!fridayEnd.after(fridayStart)) return false;
            }

            Date saturdayStart = null;
            Date saturdayEnd = null;
            if (!saturdayStartText.getText().toString().isEmpty() && !saturdayEndText.getText().toString().isEmpty()) {
                saturdayStart = timeStringToDate(saturdayStartText.getText().toString());
                saturdayEnd = timeStringToDate(saturdayEndText.getText().toString());

                if (!saturdayEnd.after(saturdayStart)) return false;
            }

            Date sundayStart = null;
            Date sundayEnd = null;
            if (!sundayStartText.getText().toString().isEmpty() && !sundayEndText.getText().toString().isEmpty()) {
                sundayStart = timeStringToDate(sundayStartText.getText().toString());
                sundayEnd = timeStringToDate(sundayEndText.getText().toString());

                if (!sundayEnd.after(sundayStart)) return false;
            }

            schedule = new EmployeeSchedule(
                    "",
                    from,
                    to,
                    mondayStart,
                    mondayEnd,
                    tuesdayStart,
                    tuesdayEnd,
                    wednesdayStart,
                    wednesdayEnd,
                    thursdayStart,
                    thursdayEnd,
                    fridayStart,
                    fridayEnd,
                    saturdayStart,
                    saturdayEnd,
                    sundayStart,
                    sundayEnd);
        } catch (Exception ignored) {
            return false;
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference userRef = db.collection("users").document(employeeId);
        DocumentReference scheduleRef = userRef.collection("schedules").document();

        schedule.setId(scheduleRef.getId());
        scheduleRef.set(schedule);

        return true;
    }

    private Date timeStringToDate(String s) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("HH:mm");
        return dt.parse(s);
    }

    private Date dateStringToDate(String s) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
        return dt.parse(s);
    }

    private boolean isDifferenceWeekMultiple(Date time1, Date time2) {
        int daysApart = (int)((time2.getTime() - time1.getTime()) / (1000*60*60*24L));
        return daysApart != 0 && abs(daysApart) % 7 == 0;
    }

    private Date localDateToDate(LocalDate ld) {
        return Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
}