package com.eventplanner.ui.employees;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeeScheduleBinding;
import com.eventplanner.databinding.FragmentEmployeeServicesBinding;
import com.eventplanner.model.CustomerService;
import com.eventplanner.model.EmployeeSchedule;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;

public class EmployeeServicesFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String employeeId;

    private FragmentEmployeeServicesBinding binding;

    private ArrayList<CustomerService> services = new ArrayList<>();

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public EmployeeServicesFragment(String employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeServicesBinding.inflate(inflater, container, false);

        db
            .collection("services")
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    services.clear();

                    for (QueryDocumentSnapshot document : task.getResult()) {
                        CustomerService service = document.toObject(CustomerService.class);

                        document.getReference().collection("providers").whereEqualTo("employeeId", employeeId).get().addOnCompleteListener(task1 -> {
                            if (task1.isSuccessful()) {
                                if (!task1.getResult().isEmpty()) {
                                    services.add(service);

                                    FragmentTransition.to(EmployeeServiceListFragment.newInstance(services), getActivity(),
                                            false, R.id.scroll_employee_service_list);
                                }
                            }
                        });
                    }
                } else {
                    Log.w("REZ_DB", "Error getting documents.", task.getException());
                }

                FragmentTransition.to(EmployeeServiceListFragment.newInstance(services), getActivity(),
                        false, R.id.scroll_employee_service_list);
            });

        return binding.getRoot();
    }
}