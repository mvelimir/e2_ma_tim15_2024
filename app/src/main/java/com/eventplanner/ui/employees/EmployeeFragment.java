package com.eventplanner.ui.employees;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeeBinding;
import com.eventplanner.model.Employee;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import de.hdodenhof.circleimageview.CircleImageView;

public class EmployeeFragment extends Fragment {
    private FragmentEmployeeBinding binding;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    public int tabPosition = 0;
    private String id;
    private Bundle bundle;

    public String getEmployeeId() { return id; }

    private CircleImageView profileImageView;
    private TextView nameText;
    private TextView emailText;
    private TextView phoneText;
    private TextView addressText;

    public EmployeeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        String identity;
        String identityField;

        bundle = getArguments();
        if (bundle != null) {
            identity = bundle.getString("employeeId");
            identityField = "id";
        } else {
            FirebaseAuth auth = FirebaseAuth.getInstance();

            identity = auth.getCurrentUser().getEmail();
            identityField = "email";
        }

        TabLayout tabLayout = view.findViewById(R.id.employee_tabs);
        FrameLayout frameLayout = view.findViewById(R.id.frame_layout);

        profileImageView = binding.profileImage;
        nameText = binding.nameText;
        emailText = binding.emailText;
        phoneText = binding.phoneText;
        addressText = binding.addressText;

        db.collection("users").whereEqualTo(identityField, identity).get().addOnSuccessListener(queryDocumentSnapshots -> {
            Employee employee = queryDocumentSnapshots.getDocuments().get(0).toObject(Employee.class);

            this.id = employee.getId();

            if (!employee.getImage().isEmpty()) {
                StorageReference profilePictureReference = storageReference.child(employee.getImage());

                final long TEN_MEGABYTES = 10 * 1024 * 1024;
                profilePictureReference.getBytes(TEN_MEGABYTES).addOnSuccessListener(bytes -> {
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    profileImageView.setImageBitmap(bmp);
                }).addOnFailureListener(exception -> Toast.makeText(getContext(), "No Such file or Path found!!", Toast.LENGTH_LONG).show());
            }

            nameText.setText(String.format("%s %s", employee.getFirstName(), employee.getLastName()));
            emailText.setText(employee.getEmail());
            phoneText.setText(employee.getPhoneNumber());
            addressText.setText(employee.getHomeAddress());

            getActionBar().setTitle(nameText.getText());

            FragmentTransition.to(new EmployeeCalendarFragment(id), getActivity(), false, R.id.frame_layout);

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    Fragment fragment = null;

                    switch (tab.getPosition()) {
                        case 0:
                            tabPosition = 0;

                            fragment = new EmployeeCalendarFragment(id);
                            break;
                        case 1:
                            tabPosition = 1;

                            fragment = new EmployeeSchedulesFragment(id);
                            break;
                        case 2:
                            tabPosition = 2;

                            fragment = new EmployeeServicesFragment(id);
                    }

                    FragmentTransition.to(fragment, getActivity(), false, R.id.frame_layout);
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        });

        return view;
    }

    private ActionBar getActionBar() {
        return ((AppCompatActivity)getActivity()).getSupportActionBar();
    }
}