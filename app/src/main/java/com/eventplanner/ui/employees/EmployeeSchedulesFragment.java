package com.eventplanner.ui.employees;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeeScheduleBinding;
import com.eventplanner.model.EmployeeSchedule;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;


public class EmployeeSchedulesFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String employeeId;

    public static ArrayList<EmployeeSchedule> schedules = new ArrayList<>();
    private FragmentEmployeeScheduleBinding binding;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public EmployeeSchedulesFragment(String employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeScheduleBinding.inflate(inflater, container, false);

        db
            .collection("users")
            .document(employeeId)
            .collection("schedules")
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    schedules.clear();

                    for (QueryDocumentSnapshot document : task.getResult()) {
                        EmployeeSchedule schedule = document.toObject(EmployeeSchedule.class);
                        schedules.add(schedule);
                    }

                    FragmentTransition.to(EmployeeScheduleListFragment.newInstance(schedules), getActivity(),
                            false, R.id.scroll_employee_schedule_list);
                } else {
                    Log.w("REZ_DB", "Error getting documents.", task.getException());
                }
            });

        return binding.getRoot();
    }
}