package com.eventplanner.ui.employees;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.eventplanner.databinding.FragmentEmployeeServicesListBinding;
import com.eventplanner.model.CustomerService;

import java.util.ArrayList;

public class EmployeeServiceListFragment extends ListFragment {
    private EmployeeServiceListAdapter adapter;
    private ArrayList<CustomerService> services;
    private FragmentEmployeeServicesListBinding binding;

    public EmployeeServiceListFragment(ArrayList<CustomerService> services) {
        this.services = services;
    }

    public static EmployeeServiceListFragment newInstance(ArrayList<CustomerService> services) {

        return new EmployeeServiceListFragment(services);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new EmployeeServiceListAdapter(getActivity(), services);
        setListAdapter(adapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEmployeeServicesListBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
