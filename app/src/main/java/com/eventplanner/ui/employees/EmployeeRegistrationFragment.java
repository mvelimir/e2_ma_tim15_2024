package com.eventplanner.ui.employees;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import android.Manifest;
import android.widget.Toast;

import com.eventplanner.databinding.FragmentEmployeeRegistrationBinding;
import com.eventplanner.model.Employee;
import com.eventplanner.model.EmployeeSchedule;
import com.eventplanner.model.Owner;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.format.DateTimeParseException;

import de.hdodenhof.circleimageview.CircleImageView;

public class EmployeeRegistrationFragment extends Fragment {

    private EmployeeRegistrationViewModel mViewModel;
    private FragmentEmployeeRegistrationBinding binding;

    private Uri profileImageUri;

    private CircleImageView profileImageView;
    private ImageButton profileImageButton;
    private EditText firstNameText;
    private EditText lastNameText;
    private EditText homeAddressText;
    private EditText phoneNumberText;
    private EditText emailText;
    private EditText passwordText;
    private EditText confirmPasswordText;
    private EditText mondayStartText;
    private EditText mondayEndText;
    private EditText tuesdayStartText;
    private EditText tuesdayEndText;
    private EditText wednesdayStartText;
    private EditText wednesdayEndText;
    private EditText thursdayStartText;
    private EditText thursdayEndText;
    private EditText fridayStartText;
    private EditText fridayEndText;
    private EditText saturdayStartText;
    private EditText saturdayEndText;
    private EditText sundayStartText;
    private EditText sundayEndText;

    private String companyId;

    private ActivityResultLauncher<Intent> galleryResultLauncher;

    public static EmployeeRegistrationFragment newInstance() {
        return new EmployeeRegistrationFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentEmployeeRegistrationBinding.inflate(inflater, container, false);

        Bundle bundle = getArguments();
        companyId = bundle.getString("companyId");

        galleryResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();

                        profileImageUri = data.getData();
                        Bitmap bitmapImage = null;
                        try {
                            bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), profileImageUri);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        this.profileImageView.setImageBitmap(bitmapImage);
                    }
                });


        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        profileImageView = binding.profileImage;
        profileImageButton = binding.profileImageButton;
        profileImageButton.setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(requireActivity(),
                    Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_MEDIA_IMAGES}, 2000);
            } else {
                startGallery();
            }
        });

        firstNameText = binding.firstNameField;
        lastNameText = binding.lastNameField;
        homeAddressText = binding.homeAddressField;
        phoneNumberText = binding.phoneNumberField;
        emailText = binding.emailField;
        passwordText = binding.passwordField;
        confirmPasswordText = binding.confirmPasswordField;
        mondayStartText = binding.mondayStartField;
        mondayEndText = binding.mondayEndField;
        tuesdayStartText = binding.tuesdayStartField;
        tuesdayEndText = binding.tuesdayEndField;
        wednesdayStartText = binding.wednesdayStartField;
        wednesdayEndText = binding.wednesdayEndField;
        thursdayStartText = binding.thursdayStartField;
        thursdayEndText = binding.thursdayEndField;
        fridayStartText = binding.fridayStartField;
        fridayEndText = binding.fridayEndField;
        saturdayStartText = binding.saturdayStartField;
        saturdayEndText = binding.saturdayEndField;
        sundayStartText = binding.sundayStartField;
        sundayEndText = binding.sundayEndField;

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("companies").document(companyId).collection("schedule").get().addOnSuccessListener(queryDocumentSnapshots -> {
            EmployeeSchedule schedule = queryDocumentSnapshots.getDocuments().get(0).toObject(EmployeeSchedule.class);

            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

            if (schedule.getMondayStart() != null) {
                mondayStartText.setText(timeFormat.format(schedule.getMondayStart()));
                mondayEndText.setText(timeFormat.format(schedule.getMondayEnd()));
            }

            if (schedule.getTuesdayStart() != null) {
                tuesdayStartText.setText(timeFormat.format(schedule.getTuesdayStart()));
                tuesdayEndText.setText(timeFormat.format(schedule.getTuesdayEnd()));
            }

            if (schedule.getWednesdayStart() != null) {
                wednesdayStartText.setText(timeFormat.format(schedule.getWednesdayStart()));
                wednesdayEndText.setText(timeFormat.format(schedule.getWednesdayEnd()));
            }

            if (schedule.getThursdayStart() != null) {
                thursdayStartText.setText(timeFormat.format(schedule.getThursdayStart()));
                thursdayEndText.setText(timeFormat.format(schedule.getThursdayEnd()));
            }

            if (schedule.getFridayStart() != null) {
                fridayStartText.setText(timeFormat.format(schedule.getFridayStart()));
                fridayEndText.setText(timeFormat.format(schedule.getFridayEnd()));
            }

            if (schedule.getSaturdayStart() != null) {
                saturdayStartText.setText(timeFormat.format(schedule.getSaturdayStart()));
                saturdayEndText.setText(timeFormat.format(schedule.getSaturdayEnd()));
            }

            if (schedule.getSundayStart() != null) {
                sundayStartText.setText(timeFormat.format(schedule.getSundayStart()));
                sundayEndText.setText(timeFormat.format(schedule.getSundayEnd()));
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(EmployeeRegistrationViewModel.class);
        // TODO: Use the ViewModel
    }

    public boolean register() {
        String firstName = firstNameText.getText().toString();
        String lastName = lastNameText.getText().toString();
        String homeAddress = homeAddressText.getText().toString();
        String phoneNumber = phoneNumberText.getText().toString();
        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();
        String confirmPassword = confirmPasswordText.getText().toString();

        EmployeeSchedule schedule;

        try {
            Date mondayStart = null;
            Date mondayEnd = null;
            if (!mondayStartText.getText().toString().isEmpty() || !mondayEndText.getText().toString().isEmpty()) {
                mondayStart = timeStringToDate(mondayStartText.getText().toString());
                mondayEnd = timeStringToDate(mondayEndText.getText().toString());

                if (!mondayEnd.after(mondayStart)) return false;
            }

            Date tuesdayStart = null;
            Date tuesdayEnd = null;
            if (!tuesdayStartText.getText().toString().isEmpty() && !tuesdayEndText.getText().toString().isEmpty()) {
                tuesdayStart = timeStringToDate(tuesdayStartText.getText().toString());
                tuesdayEnd = timeStringToDate(tuesdayEndText.getText().toString());

                if (!tuesdayEnd.after(tuesdayStart)) return false;
            }

            Date wednesdayStart = null;
            Date wednesdayEnd = null;
            if (!wednesdayStartText.getText().toString().isEmpty() && !wednesdayEndText.getText().toString().isEmpty()) {
                wednesdayStart = timeStringToDate(wednesdayStartText.getText().toString());
                wednesdayEnd = timeStringToDate(wednesdayEndText.getText().toString());

                if (!wednesdayEnd.after(wednesdayStart)) return false;
            }

            Date thursdayStart = null;
            Date thursdayEnd = null;
            if (!thursdayStartText.getText().toString().isEmpty() && !thursdayEndText.getText().toString().isEmpty()) {
                thursdayStart = timeStringToDate(thursdayStartText.getText().toString());
                thursdayEnd = timeStringToDate(thursdayEndText.getText().toString());

                if (!thursdayEnd.after(thursdayStart)) return false;
            }

            Date fridayStart = null;
            Date fridayEnd = null;
            if (!fridayStartText.getText().toString().isEmpty() && !fridayEndText.getText().toString().isEmpty()) {
                fridayStart = timeStringToDate(fridayStartText.getText().toString());
                fridayEnd = timeStringToDate(fridayEndText.getText().toString());

                if (!fridayEnd.after(fridayStart)) return false;
            }

            Date saturdayStart = null;
            Date saturdayEnd = null;
            if (!saturdayStartText.getText().toString().isEmpty() && !saturdayEndText.getText().toString().isEmpty()) {
                saturdayStart = timeStringToDate(saturdayStartText.getText().toString());
                saturdayEnd = timeStringToDate(saturdayEndText.getText().toString());

                if (!saturdayEnd.after(saturdayStart)) return false;
            }

            Date sundayStart = null;
            Date sundayEnd = null;
            if (!sundayStartText.getText().toString().isEmpty() && !sundayEndText.getText().toString().isEmpty()) {
                sundayStart = timeStringToDate(sundayStartText.getText().toString());
                sundayEnd = timeStringToDate(sundayEndText.getText().toString());

                if (!sundayEnd.after(sundayStart)) return false;
            }

            schedule = new EmployeeSchedule(
                    "",
                    new Date(),
                    mondayStart,
                    mondayEnd,
                    tuesdayStart,
                    tuesdayEnd,
                    wednesdayStart,
                    wednesdayEnd,
                    thursdayStart,
                    thursdayEnd,
                    fridayStart,
                    fridayEnd,
                    saturdayStart,
                    saturdayEnd,
                    sundayStart,
                    sundayEnd);
        } catch (Exception ignored) {
            return false;
        }

        if (!password.equals(confirmPassword)) return false;
        if (firstName.isEmpty()) return false;
        if (lastName.isEmpty()) return false;
        if (homeAddress.isEmpty()) return false;
        if (phoneNumber.isEmpty()) return false;
        if (email.isEmpty()) return false;
        if (!isValidEmail(email)) return false;
        if (password.isEmpty() || password.length() < 6) return false;

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseStorage storage = FirebaseStorage.getInstance();

        String image = "";

        StorageReference storageRef = storage.getReference();
        if (profileImageUri != null) {
            image = "images/" + getFileName(profileImageUri);
            StorageReference profilePicRef = storageRef.child(image);
            profileImageView.setDrawingCacheEnabled(true);
            profileImageView.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) profileImageView.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            profilePicRef.putBytes(data);
        }

        DocumentReference userRef = db.collection("users").document();
        DocumentReference scheduleRef = userRef.collection("schedules").document();

        Employee employee = new Employee(userRef.getId(), email, password, firstName, lastName, homeAddress, phoneNumber, image, companyId, false);
        schedule.setId(scheduleRef.getId());

        userRef.set(employee);
        scheduleRef.set(schedule);

        FirebaseAuth auth = FirebaseAuth.getInstance();

        auth
            .createUserWithEmailAndPassword(employee.getEmail(), employee.getPassword())
            .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        Log.d("TAG", "createUserWithEmail:success");
                        FirebaseUser user = auth.getCurrentUser();

                        user.sendEmailVerification();

                        auth.signOut();
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("TAG", "createUserWithEmail:failure", task.getException());
                        Toast.makeText(getActivity(), "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });

        return true;
    }

    private void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK);
        cameraIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        galleryResultLauncher.launch(cameraIntent);
    }

    @SuppressLint("Range")
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public static boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private Date timeStringToDate(String s) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("HH:mm");
        return dt.parse(s);
    }

}