package com.eventplanner.ui.employees;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.eventplanner.databinding.FragmentEmployeeEventFormBinding;
import com.eventplanner.model.Employee;
import com.eventplanner.model.EmployeeEvent;
import com.eventplanner.model.EmployeeEventCategory;
import com.eventplanner.model.User;
import com.eventplanner.model.UserType;
import com.eventplanner.services.UserService;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EmployeeEventFormFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeEventFormFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public ArrayList<EmployeeEvent> events = new ArrayList<>();

    private FragmentEmployeeEventFormBinding binding;

    private EditText nameText;
    private EditText dayText;
    private EditText startTimeText;
    private EditText endTimeText;

    private String employeeId;

    private String ownerId;

    private EmployeeEventCategory eventCategory;

    public String getEmployeeId() { return employeeId; }

    public String getOwnerId() { return ownerId; }

    public EmployeeEventFormFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmployeeEventFormFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmployeeEventFormFragment newInstance(String param1, String param2) {
        EmployeeEventFormFragment fragment = new EmployeeEventFormFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeEventFormBinding.inflate(inflater, container, false);

        Bundle bundle = getArguments();
        employeeId = bundle.getString("employeeId");

        UserService userService = new UserService();

        userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
            User user = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class);

            if (user.getType() == UserType.OWNER) {
                eventCategory = EmployeeEventCategory.BUSY;
            } else {
                Employee employee = queryDocumentSnapshots.getDocuments().get(0).toObject(Employee.class);

                db.collection("users").whereEqualTo("type", UserType.OWNER).whereEqualTo("companyId", employee.getCompanyId()).get().addOnSuccessListener(queryDocumentSnapshots1 -> {
                    User user1 = queryDocumentSnapshots1.getDocuments().get(0).toObject(User.class);

                    ownerId = user1.getId();
                });

                eventCategory = EmployeeEventCategory.BUSY;
            }
        });

        db
            .collection("users")
            .document(employeeId)
            .collection("events")
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    events.clear();

                    for (QueryDocumentSnapshot document : task.getResult()) {
                        EmployeeEvent event = document.toObject(EmployeeEvent.class);
                        events.add(event);
                    }
                } else {
                    Log.w("REZ_DB", "Error getting documents.", task.getException());
                }
            });

        nameText = binding.eventNameField;
        dayText = binding.eventDayField;
        startTimeText = binding.eventStartTimeField;
        endTimeText = binding.eventEndTimeField;

        return binding.getRoot();
    }

    public boolean register() {
        try {
            String name = nameText.getText().toString();
            Date day = dateStringToDate(dayText.getText().toString());
            Date start = timeStringToDate(startTimeText.getText().toString());
            Date end = timeStringToDate(endTimeText.getText().toString());

            for (EmployeeEvent event : events) {
                if (isSameDay(day, event.getDate())) {
                    if (!(!end.after(event.getFrom()) || !start.before(event.getTo()))) {
                        return false;
                    }
                }
            }

            if (isSameDay(new Date(), day) && !isTimeAfter(start, new Date())) return false;
            if (!day.after(localDateToDate(LocalDate.now().minusDays(1)))) return false;
            if (name.isEmpty()) return false;
            if (!end.after(start)) return false;

            EmployeeEvent event = new EmployeeEvent("", name, day, start, end, eventCategory);

            FirebaseFirestore db = FirebaseFirestore.getInstance();

            DocumentReference userRef = db.collection("users").document(employeeId);
            DocumentReference eventRef = userRef.collection("events").document();

            event.setId(eventRef.getId());
            eventRef.set(event);
        } catch (Exception ignored) {
            return false;
        }

        return true;
    }

    private Date timeStringToDate(String s) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("HH:mm");
        return dt.parse(s);
    }

    private Date dateStringToDate(String s) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
        return dt.parse(s);
    }

    private Date localDateToDate(LocalDate ld) {
        return Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static boolean isTimeAfter(Date date1, Date date2) {
        // Extract hours and minutes from Date objects
        int hours1 = date1.getHours();
        int minutes1 = date1.getMinutes();

        int hours2 = date2.getHours();
        int minutes2 = date2.getMinutes();

        // Compare hours and minutes
        if (hours1 > hours2) {
            return true;
        } else if (hours1 == hours2 && minutes1 > minutes2) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isTimeAfterOrEqual(Date date1, Date date2) {
        // Extract hours and minutes from Date objects
        int hours1 = date1.getHours();
        int minutes1 = date1.getMinutes();

        int hours2 = date2.getHours();
        int minutes2 = date2.getMinutes();

        // Compare hours and minutes
        if (hours1 > hours2) {
            return true;
        } else if (hours1 == hours2 && minutes1 >= minutes2) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
    }
}