package com.eventplanner.ui.employees;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.eventplanner.R;
import com.eventplanner.model.EmployeeEvent;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class EmployeeEventListAdapter extends ArrayAdapter<EmployeeEvent>  {
    public EmployeeEventListAdapter(Context context, ArrayList<EmployeeEvent> employees) {
        super(context, 0, employees);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        EmployeeEvent event = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.employee_event_card, parent, false);
        }

        TextView name = convertView.findViewById(R.id.event_name);
        TextView date = convertView.findViewById(R.id.event_date);
        TextView range = convertView.findViewById(R.id.event_time_range);
        TextView category = convertView.findViewById(R.id.event_category);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

        name.setText(event.getName());
        date.setText(dateFormat.format(event.getDate()));
        range.setText(timeFormat.format(event.getFrom()) + " - " + timeFormat.format(event.getTo()));
        category.setText(event.getCategory().toString().toLowerCase());

        return convertView;
    }
}
