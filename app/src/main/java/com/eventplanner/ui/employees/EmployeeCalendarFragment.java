package com.eventplanner.ui.employees;

import static com.eventplanner.ui.employees.CalendarUtils.daysInWeekArray;
import static com.eventplanner.ui.employees.CalendarUtils.monthYearFromDate;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeeCalendarBinding;
import com.eventplanner.model.Employee;
import com.eventplanner.model.EmployeeEvent;
import com.eventplanner.model.EmployeeEventCategory;
import com.eventplanner.model.EmployeeSchedule;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.sql.Time;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

public class EmployeeCalendarFragment extends Fragment implements CalendarAdapter.OnItemListener {
    public static ArrayList<EmployeeEvent> events = new ArrayList<EmployeeEvent>();
    private TextView monthYearText;
    private RecyclerView calendarRecyclerView;
    private FragmentEmployeeCalendarBinding binding;

    private ArrayList<LocalDate> days;

    private String employeeId;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public EmployeeCalendarFragment(String employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CalendarUtils.pickedDate = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeCalendarBinding.inflate(inflater, container, false);

        initWidgets();
        setWeekView();
        getEventsForCurrentWeek();

        return binding.getRoot();
    }

    private void initWidgets()
    {
        binding.previousWeekButton.setOnClickListener(this::previousWeekAction);
        binding.nextWeekButton.setOnClickListener(this::nextWeekAction);
        calendarRecyclerView = binding.calendarRecyclerView;
        monthYearText = binding.monthYearTV;
    }

    private void setWeekView()
    {
        monthYearText.setText(monthYearFromDate(CalendarUtils.referenceDate));
        days = daysInWeekArray(CalendarUtils.referenceDate);

        CalendarAdapter calendarAdapter = new CalendarAdapter(days, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 7);
        calendarRecyclerView.setLayoutManager(layoutManager);
        calendarRecyclerView.setAdapter(calendarAdapter);
    }

    public void previousWeekAction(View view)
    {
        CalendarUtils.referenceDate = CalendarUtils.referenceDate.minusWeeks(1);
        CalendarUtils.pickedDate = null;
        setWeekView();
        getEventsForCurrentWeek();
    }

    public void nextWeekAction(View view)
    {
        CalendarUtils.referenceDate = CalendarUtils.referenceDate.plusWeeks(1);
        CalendarUtils.pickedDate = null;
        setWeekView();
        getEventsForCurrentWeek();
    }

    @Override
    public void onItemClick(int position, LocalDate date)
    {
        CalendarUtils.referenceDate = date;
        if (date.equals(CalendarUtils.pickedDate)) {
            CalendarUtils.pickedDate = null;
            getEventsForCurrentWeek();
        } else {
            CalendarUtils.pickedDate = date;
            getEventsForCurrentDay();
        }

        setWeekView();
    }

    private Date localDateToDate(LocalDate ld) {
        return Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private void getEventsForCurrentWeek() {
        db
            .collection("users")
            .document(employeeId)
            .collection("events")
            .whereGreaterThanOrEqualTo("date", localDateToDate(days.get(0)))
            .whereLessThan("date", localDateToDate(days.get(6).plusDays(1)))
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    events.clear();

                    for (QueryDocumentSnapshot document : task.getResult()) {
                        EmployeeEvent event = document.toObject(EmployeeEvent.class);
                        events.add(event);
                    }

                    FragmentTransition.to(EmployeeEventListFragment.newInstance(events), getActivity(),
                            false, R.id.scroll_employee_events_list);
                } else {
                    Log.w("REZ_DB", "Error getting documents.", task.getException());
                }
            });
    }

    private void getEventsForCurrentDay() {
        db
            .collection("users")
            .document(employeeId)
            .collection("events")
            .whereGreaterThanOrEqualTo("date", localDateToDate(CalendarUtils.pickedDate))
            .whereLessThan("date", localDateToDate(CalendarUtils.pickedDate.plusDays(1)))
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    events.clear();

                    for (QueryDocumentSnapshot document : task.getResult()) {
                        EmployeeEvent event = document.toObject(EmployeeEvent.class);
                        events.add(event);
                    }

                    FragmentTransition.to(EmployeeEventListFragment.newInstance(events), getActivity(),
                            false, R.id.scroll_employee_events_list);
                } else {
                    Log.w("REZ_DB", "Error getting documents.", task.getException());
                }
            });
    }

}