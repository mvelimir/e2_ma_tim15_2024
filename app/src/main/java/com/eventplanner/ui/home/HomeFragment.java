package com.eventplanner.ui.home;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.eventplanner.MainActivity;
import com.eventplanner.MessagingService;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentHomeBinding;
import com.eventplanner.model.Employee;
import com.eventplanner.model.Notification;
import com.eventplanner.model.User;
import com.eventplanner.model.UserType;
import com.eventplanner.repositories.NotificationRepository;
import com.eventplanner.services.UserService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;

public class HomeFragment extends Fragment {
    private FragmentHomeBinding binding;
    private Button loginButton;
    private String mEmail;
    private String mPassword;
    private FirebaseUser fbLoggedUser = null;
    private UserService userService;
    private User loggedUser = null;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    private TextView RegisterAsEventOrganizer;
    private TextView RegisterAsOwner;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        HomeViewModel homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);

        RegisterAsEventOrganizer = binding.registerEventOrganizerTextView;
        RegisterAsOwner = binding.registerOwnerTextView;

        RegisterAsEventOrganizer.setOnClickListener(v -> {
            NavController navController = Navigation.findNavController(v);
            navController.navigate(R.id.registerUserFragment);
        });
        RegisterAsOwner.setOnClickListener(v -> {
            NavController navController = Navigation.findNavController(v);
            navController.navigate(R.id.registerOwnerFragment);
        });

        loginButton = binding.loginButton;
        userService = new UserService();
        mAuth = FirebaseAuth.getInstance();

        loginButton.setOnClickListener(v -> {
            mEmail = binding.email.getText().toString().trim();
            mPassword = binding.password.getText().toString().trim();

            if (mEmail == null || mEmail.isEmpty()) {
                Log.e("TAG", "Email is empty or null");
                Toast.makeText(getActivity(), "Email is empty or null", Toast.LENGTH_SHORT).show();
                return;
            }

            if (mPassword.isEmpty()) {
                Log.e("TAG", "Password is empty or null");
                Toast.makeText(getActivity(), "Password is empty or null", Toast.LENGTH_SHORT).show();
                return;
            }

            mAuth.signInWithEmailAndPassword(mEmail, mPassword)
                    .addOnCompleteListener(task -> {
                        if (!isAdded()) {
                            Log.d("TAG", "Fragment not attached to activity");
                            return;
                        }

                        if (task.isSuccessful()) {
                            Log.d("TAG", "signInWithEmailAndPassword: success");

                            fbLoggedUser = mAuth.getCurrentUser();

                            if (fbLoggedUser != null) {
                                fbLoggedUser.getIdToken(true).addOnCompleteListener(tokenTask -> {
                                    if (tokenTask.isSuccessful()) {
                                        String idToken = tokenTask.getResult().getToken();
                                        Log.d("TAG", "ID Token: " + idToken);
                                    } else {
                                        Log.e("TAG", "Error getting ID token: " + tokenTask.getException());
                                    }
                                });
                            }

                            userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
                                if (!queryDocumentSnapshots.isEmpty()) {
                                    User user = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class);
                                    Log.d("TAG", "User Type: " + user.getType());

                                    if (user.getType() == UserType.EMPLOYEE) {
                                        if (!mAuth.getCurrentUser().isEmailVerified()) {
                                            Toast.makeText(getActivity(), "You must verify your email first", Toast.LENGTH_SHORT).show();
                                            mAuth.signOut();
                                            return;
                                        }
                                    } else if (user.getType() == UserType.ADMIN) {
                                        NavController navController = Navigation.findNavController(v);
                                        navController.navigate(R.id.companyRegistrationRequestsFragment);
                                    }

                                    UserService.email = fbLoggedUser.getEmail();
                                    UserService.password = mPassword;

                                    NotificationRepository.getNotificationsForUser(user.getId(), notifications -> {
                                        for (Notification n : notifications) {
                                            String title = n.getTitle();
                                            String messageBody = n.getMessage();
                                            Intent intent = new Intent(getActivity(), MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, PendingIntent.FLAG_IMMUTABLE);

                                            String channelId = "eventplanner";
                                            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getActivity(), channelId)
                                                    .setSmallIcon(R.drawable.ic_planner)
                                                    .setContentTitle(title)
                                                    .setContentText(messageBody)
                                                    .setAutoCancel(true)
                                                    .setSound(defaultSoundUri)
                                                    .setContentIntent(pendingIntent);

                                            NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                                NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
                                                notificationManager.createNotificationChannel(channel);
                                            }

                                            notificationManager.notify(n.getId(), 0, notificationBuilder.build());
                                        }
                                    });

                                    FirebaseMessaging.getInstance().getToken()
                                            .addOnCompleteListener(new OnCompleteListener<String>() {
                                                @Override
                                                public void onComplete(@NonNull Task<String> task) {
                                                    if (!task.isSuccessful()) {
                                                        Log.w("FCM", "Fetching FCM registration token failed", task.getException());
                                                        return;
                                                    }

                                                    String token = task.getResult();
                                                    Log.d("FCM", "FCM Token: " + token);
                                                }
                                            });

                                    MainActivity activity = (MainActivity) getActivity();
                                    activity.setNavView();
                                } else {
                                    Log.e("TAG", "No user found");
                                    Toast.makeText(getActivity(), "No user found", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(e -> {
                                Log.e("TAG", "Error getting user: " + e.getMessage());
                                Toast.makeText(getActivity(), "Error getting user: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            });
                        } else {
                            Log.w("TAG", "signInWithEmail: failure", task.getException());
                            Toast.makeText(getActivity(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    });
        });

        //users = new ArrayList<>();

        //d//b.collection("users").get().addOnCompleteListener(this::populateList);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    /*private void populateList(Task<QuerySnapshot> task) {
        if (task.isSuccessful()) {
            users.clear();

            for (QueryDocumentSnapshot document : task.getResult()) {
                User user = document.toObject(User.class);
                if (user.getType() == UserType.OWNER) {
                    users.add(user);
                } else {
                    Employee employee = document.toObject(Employee.class);

                    if (!employee.isBlocked()) {
                        users.add(employee);
                    }
                }
            }

            String[] usernames = users.stream().map(User::getEmail).toArray(String[]::new);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, usernames);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        } else {
            Log.w("REZ_DB", "Error getting documents.", task.getException());
        }
    }*/
}
