package com.eventplanner.ui.owners;

import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.util.Log;
import androidx.core.util.Pair;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentOwnerProfileBinding;
import com.eventplanner.model.CompanyReview;
import com.eventplanner.model.Owner;
import com.eventplanner.services.CompanyReviewService;
import com.eventplanner.services.UserService;
import com.eventplanner.ui.companies.CompanyReviewListFragment;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.TimeZone;
import java.util.stream.Collectors;

public class OwnerProfileFragment extends Fragment {

    private ArrayList<CompanyReview> reviews;
    private ArrayList<CompanyReview> filteredReviews;

    private FragmentOwnerProfileBinding binding;
    private Button btnSelectStartDate, btnSelectEndDate, btnReset;
    private TextView filterDateRange;

    String companyId;

    private CompanyReviewService companyReviewService;
    private UserService userService;

    private String ownerId;

    private Long startDate, endDate;

    public OwnerProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ownerId = getArguments().getString("ownerId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentOwnerProfileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        btnSelectStartDate = binding.selectDateStartButton;
        btnSelectEndDate = binding.selectDateEndButton;
        btnReset = binding.resetButton;
        filterDateRange = binding.filterDateRange;

        btnSelectStartDate.setOnClickListener(v -> showStartDatePicker());
        btnSelectEndDate.setOnClickListener(v -> showEndDatePicker());
        btnReset.setOnClickListener(v -> resetDateRange());

        companyReviewService = new CompanyReviewService();
        userService = new UserService();

        if (ownerId != null) {
            userService.get(ownerId, Owner.class, owner -> {
                companyId = owner.getCompanyId();

                getActionBar().setTitle(owner.getFirstName() + " " + owner.getLastName());

                if (companyId == null) return;

                companyReviewService.getAllForCompany(companyId, rs -> {
                    reviews = rs;
                    filterReviews();
                });
            });
        } else {
            userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
                Owner owner = queryDocumentSnapshots.getDocuments().get(0).toObject(Owner.class);

                getActionBar().setTitle(owner.getFirstName() + " " + owner.getLastName());

                companyId = owner.getCompanyId();

                if (companyId == null) return;

                companyReviewService.getAllForCompany(companyId, rs -> {
                    reviews = rs;
                    filterReviews();
                });
            });
        }

        return root;
    }

    private void filterReviews() {
        filteredReviews = reviews.stream().filter(this::isReviewInDateRange).collect(Collectors.toCollection(ArrayList::new));

        FragmentTransition.to(CompanyReviewListFragment.newInstance(filteredReviews), getActivity(),
                false, R.id.scroll_company_reviews_list);
    }

    private boolean isReviewInDateRange(CompanyReview review) {
        LocalDate startDateLocal = startDate == null ? null : Instant.ofEpochMilli(startDate)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        LocalDate endDateLocal = endDate == null ? null : Instant.ofEpochMilli(endDate)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        LocalDate reviewDate = Instant.ofEpochMilli(review.getDate().getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        return (startDateLocal == null || !reviewDate.isBefore(startDateLocal)) && (endDateLocal == null || !reviewDate.isAfter(endDateLocal));
    }

    private void showStartDatePicker() {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText("Select start date");

        final MaterialDatePicker<Long> startDatePicker = builder.build();
        FragmentManager fragmentManager = getParentFragmentManager();

        if (fragmentManager != null) {
            startDatePicker.show(fragmentManager, startDatePicker.toString());

            startDatePicker.addOnPositiveButtonClickListener(selection -> {
                startDate = selection;
                updateDateRange();
                filterReviews();
            });
        }
    }

    private ActionBar getActionBar() {
        return ((AppCompatActivity)getActivity()).getSupportActionBar();
    }

    private void showEndDatePicker() {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText("Select end date");

        final MaterialDatePicker<Long> endDatePicker = builder.build();
        FragmentManager fragmentManager = getParentFragmentManager();

        if (fragmentManager != null) {
            endDatePicker.show(fragmentManager, endDatePicker.toString());

            endDatePicker.addOnPositiveButtonClickListener(selection -> {
                endDate = selection;
                updateDateRange();
                filterReviews();
            });
        }
    }

    private void updateDateRange() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String startDateString = (startDate != null) ? sdf.format(startDate) : "";
        String endDateString = (endDate != null) ? sdf.format(endDate) : "";

        String dateRange = startDateString + " - " + endDateString;
        filterDateRange.setText(dateRange);
    }

    private void resetDateRange() {
        startDate = null;
        endDate = null;
        filterDateRange.setText("");

        if (companyId == null) return;

        filterReviews();
    }


}