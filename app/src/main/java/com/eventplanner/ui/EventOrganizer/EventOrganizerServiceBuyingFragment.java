package com.eventplanner.ui.EventOrganizer;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.eventplanner.R;
import com.eventplanner.model.EmployeeEvent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class EventOrganizerServiceBuyingFragment extends Fragment {

    private ListView eventListView;
    private EmployeeEventAdapter eventAdapter;
    private List<EmployeeEvent> eventList;
    private FirebaseFirestore db;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public EventOrganizerServiceBuyingFragment() {
        // Required empty public constructor
    }

    public static EventOrganizerServiceBuyingFragment newInstance(String param1, String param2) {
        EventOrganizerServiceBuyingFragment fragment = new EventOrganizerServiceBuyingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.event_organizer_service_buying_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        eventListView = view.findViewById(R.id.event_list_view);
        eventList = new ArrayList<>();
        eventAdapter = new EmployeeEventAdapter(getContext(), eventList);
        eventListView.setAdapter(eventAdapter);

        db = FirebaseFirestore.getInstance();

        fetchEvents();
    }

    private void fetchEvents() {
        db.collection("events")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            eventList.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                EmployeeEvent event = document.toObject(EmployeeEvent.class);
                                eventList.add(event);
                            }
                            eventAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(getContext(), "Error getting events.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
