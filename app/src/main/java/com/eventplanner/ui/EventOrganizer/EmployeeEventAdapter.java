package com.eventplanner.ui.EventOrganizer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.eventplanner.R;
import com.eventplanner.model.EmployeeEvent;

import java.util.List;

public class EmployeeEventAdapter extends ArrayAdapter<EmployeeEvent> {

    private Context context;
    private List<EmployeeEvent> events;

    public EmployeeEventAdapter(Context context, List<EmployeeEvent> events) {
        super(context, R.layout.item_event, events);
        this.context = context;
        this.events = events;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_event, parent, false);
        }

        EmployeeEvent event = events.get(position);

        TextView eventName = convertView.findViewById(R.id.event_name);
        TextView eventBudget = convertView.findViewById(R.id.event_budget);
        TextView remainingBudget = convertView.findViewById(R.id.event_remaining_budget);
        Button buyProductButton = convertView.findViewById(R.id.button_buy_product);

        eventName.setText(event.getName());
        eventBudget.setText("Event Budget: $" + event.getEventBudget());
        remainingBudget.setText("Remaining Budget: $" + event.getRemainingBudget());

        buyProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Buy Product for event: " + event.getName(), Toast.LENGTH_SHORT).show();
                // Handle the buy product action here
            }
        });

        return convertView;
    }
}
