package com.eventplanner.ui.EventOrganizer;

import static com.eventplanner.R.id.profile_image;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.eventplanner.R;
import com.eventplanner.databinding.FragmentRegisterEventOrganizerBinding;
import com.eventplanner.model.EventOrganizer;
import com.eventplanner.model.UserType;
import com.eventplanner.services.UserService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class RegisterEventOrganizerFragment extends Fragment {

    private UserService service;
    private FragmentRegisterEventOrganizerBinding binding;
    private Uri imageUri;

    private de.hdodenhof.circleimageview.CircleImageView profileImageView;
    private Button registerButton;
    private ImageButton profileImageButton;
    private ActivityResultLauncher<Intent> galleryResultLauncher;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public RegisterEventOrganizerFragment() {
        service = new UserService();
    }

    public static RegisterEventOrganizerFragment newInstance(String param1, String param2) {
        RegisterEventOrganizerFragment fragment = new RegisterEventOrganizerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentRegisterEventOrganizerBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        galleryResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        imageUri = data.getData();
                        Bitmap bitmapImage = null;
                        try {
                            bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                            binding.profileImage.setImageBitmap(bitmapImage);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        profileImageButton = binding.choosePictureButton;
        registerButton = binding.registerButton;
        profileImageView = binding.profileImage;

        profileImageButton.setOnClickListener(view -> {
            if (ContextCompat.checkSelfPermission(requireActivity(),
                    Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_MEDIA_IMAGES}, 2000);
            } else {
                startGallery();
            }
        });

        registerButton.setOnClickListener(view -> {
            boolean isRegistered = registerEventProvider();
            if (isRegistered) {
                // Clear the form fields after successful registration
                binding.email.setText("");
                binding.password.setText("");
                binding.confirmPassword.setText("");
                binding.name.setText("");
                binding.surname.setText("");
                binding.address.setText("");
                binding.phone.setText("");
                //binding.profileImage.setImageResource(); // Reset to default image
            }
        });
    }

    public boolean registerEventProvider() {
        String email = binding.email.getText().toString();
        String password = binding.password.getText().toString();
        String name = binding.name.getText().toString();
        String surname = binding.surname.getText().toString();
        String address = binding.address.getText().toString();
        String phone = binding.phone.getText().toString();
        String confirmPassword = binding.confirmPassword.getText().toString();

        if (!password.equals(confirmPassword)) {
            Toast.makeText(getActivity(), "Passwords do not match.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (name.isEmpty() || surname.isEmpty() || address.isEmpty() || phone.isEmpty() || email.isEmpty() || !isValidEmail(email)) {
            Toast.makeText(getActivity(), "Please fill out all fields correctly.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.isEmpty() || password.length() < 6) {
            Toast.makeText(getActivity(), "Password must be at least 6 characters long.", Toast.LENGTH_SHORT).show();
            return false;
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseStorage storage = FirebaseStorage.getInstance();

        String image = "";
        StorageReference storageRef = storage.getReference();

        if (imageUri != null) {
            image = "images/" + getFileName(imageUri);
            StorageReference profileImageRef = storageRef.child(image);
            profileImageView.setDrawingCacheEnabled(true);
            profileImageView.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) profileImageView.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            profileImageRef.putBytes(data);
        }

        DocumentReference eventOrganizerRef = db.collection("eventOrganizers").document();
        EventOrganizer organizer = new EventOrganizer(eventOrganizerRef.getId(), email, password, name, surname, address, phone, image, UserType.EVENT_ORGANIZER, false);
        eventOrganizerRef.set(organizer);

        FirebaseAuth auth = FirebaseAuth.getInstance();

        auth.createUserWithEmailAndPassword(organizer.getEmail(), organizer.getPassword())
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("TAG", "createUserWithEmail:success");
                            FirebaseUser user = auth.getCurrentUser();

                            user.sendEmailVerification();
                            auth.signOut();
                            Toast.makeText(getActivity(), "Event organizer successfully registered", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.w("TAG", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getActivity(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        return true;
    }

    @SuppressLint("Range")
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK);
        cameraIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        galleryResultLauncher.launch(cameraIntent);
    }

    public static boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void updateRegistrationConfirmed(String uid) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("eventOrganizers").document(uid)
                .update("isRegistrationConfirmed", true)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getActivity(), "Email verified successfully.", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.w("TAG", "Error updating registration confirmation: " + task.getException());
                            Toast.makeText(getActivity(), "Failed to update registration confirmation.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
