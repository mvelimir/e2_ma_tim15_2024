package com.eventplanner.ui.customerService;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.eventplanner.adapters.CustomerServiceListAdapter;
import com.eventplanner.databinding.FragmentOwnerServiceListBinding;
import com.eventplanner.databinding.FragmentServiceListBinding;
import com.eventplanner.model.CustomerService;

import java.util.ArrayList;

public class ServiceListFragment extends ListFragment {

    private CustomerServiceListAdapter adapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<CustomerService> mServices;
    private FragmentServiceListBinding binding;

    public static ServiceListFragment newInstance(ArrayList<CustomerService> services){
        ServiceListFragment fragment = new ServiceListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, services);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.i("ShopApp", "onCreate Service List Fragment");
        if (getArguments() != null) {
            mServices = getArguments().getParcelableArrayList(ARG_PARAM);
            adapter = new CustomerServiceListAdapter(getActivity(), mServices);
            setListAdapter(adapter);
        }
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i("ShopApp", "onCreateView Services List Fragment");
        binding = FragmentServiceListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
