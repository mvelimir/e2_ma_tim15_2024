package com.eventplanner.ui.customerService;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeeServiceListBinding;
import com.eventplanner.model.CustomerService;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;

public class EmployeeServiceListFragment extends Fragment {

    private FragmentEmployeeServiceListBinding binding;
    public static ArrayList<CustomerService> services = new ArrayList<CustomerService>();
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.FullScreenBottomSheetDialog);
                View dialogView = getLayoutInflater().inflate(R.layout.bottom_sheet_filter_services, null);
                bottomSheetDialog.setContentView(dialogView);
                bottomSheetDialog.show();
                //Navigation.findNavController(view).navigate(R.id.action_nav_products_owner_to_nav_filter);
            }
        });
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        createServices(services);
        binding = FragmentEmployeeServiceListBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        FragmentTransition.to(ServiceListFragment.newInstance(services), getActivity(),
                false, R.id.scroll_services_list);
        return view;
    }

    public void createServices(ArrayList<CustomerService> products) {

        // Example 1
        CustomerService service1 = new CustomerService(
                "1",
                "Wedding Catering",
                "Premium Wedding Catering Service",
                "Exquisite catering service for weddings",
                "1500",
                "2500",
                "6 hours",
                "Venue1",
                "Discount available for early bookings",
                "Full service with servers included",
                "2024-09-30",
                "2024-09-15",
                "10%",
                "2250",
                "image1",
                "Customizable menu options",
                "Automatic acceptance",
                "Wedding",
                true,
                true
        );

// Example 2
        CustomerService service2 = new CustomerService(
                "2",
                "Event Photography",
                "Professional Event Photography Service",
                "Capture memorable moments with our photography service",
                "100",
                "100",
                "4 hours",
                "Venue2",
                "No discount available",
                "Photographer will bring own equipment",
                "2024-10-30",
                "2024-10-25",
                "0%",
                "100",
                "image2",
                "High-resolution digital images provided",
                "Manual",
                "Event",
                true,
                true
        );

// Example 3
        CustomerService service3 = new CustomerService(
                "3",
                "Live Music",
                "Live Band Performance",
                "Energize your event with live music",
                "800",
                "760",
                "3 hours",
                "Venue3",
                "Discount available for weekday bookings",
                "Full band setup with sound system",
                "2024-11-30",
                "2024-11-20",
                "5%",
                "722",
                "image3",
                "Customizable playlist",
                "Automatic acceptance",
                "Party",
                true,
                true
        );

// Example 4
        CustomerService service4 = new CustomerService(
                "4",
                "Limo Service",
                "Luxury Limo Service",
                "Arrive in style with our luxury limousine service",
                "500",
                "500",
                "2 hours",
                "Venue4",
                "No discount available",
                "Professional chauffeur provided",
                "2024-12-30",
                "2024-12-20",
                "0%",
                "500",
                "image4",
                "Complimentary beverages included",
                "Manual",
                "Special Occasion",
                true,
                true
        );

// Example 5
        CustomerService service5 = new CustomerService(
                "5",
                "Event Decoration",
                "Event Decoration Service",
                "Transform your venue with our event decoration service",
                "300",
                "255",
                "4 hours",
                "Venue5",
                "Discount available for bulk bookings",
                "Professional decorators with creative themes",
                "2025-01-30",
                "2025-01-25",
                "15%",
                "255",
                "image5",
                "Customizable themes and setups",
                "Manual",
                "Event",
                true,
                true
        );


        // Adding products to the list
        services.add(service1);
        services.add(service2);
        services.add(service3);
        services.add(service4);
        services.add(service5);
    }
}