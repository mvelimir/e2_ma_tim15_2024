package com.eventplanner.ui.eventPackages;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeeEventPackageListBinding;
import com.eventplanner.databinding.FragmentOwnerEventPackageListBinding;
import com.eventplanner.model.CustomerService;
import com.eventplanner.model.EventPackage;
import com.eventplanner.model.Product;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;


public class EmployeeEventPackageListFragment extends Fragment {

    private FragmentEmployeeEventPackageListBinding binding;
    public static ArrayList<EventPackage> packages = new ArrayList<EventPackage>();
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.FullScreenBottomSheetDialog);
                View dialogView = getLayoutInflater().inflate(R.layout.bottom_sheet_filter_packages, null);
                bottomSheetDialog.setContentView(dialogView);
                bottomSheetDialog.show();
                //Navigation.findNavController(view).navigate(R.id.action_nav_products_owner_to_nav_filter);
            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        createPackages(packages);
        binding = FragmentEmployeeEventPackageListBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        FragmentTransition.to(EventPackageListFragment.newInstance(packages), getActivity(),
                false, R.id.scroll_package_list);
        return view;
    }

    public void createPackages(ArrayList<EventPackage> packages) {

        // Creating 5 instances of Product
// Example 1
        EventPackage example1 = new EventPackage(
                "Wedding Package",
                "This package includes everything you need to decorate your wedding venue.",
                "10",
                true,
                true,
                "Decoration",
                "Wedding",
                "5000",
                "wedding_decor.jpg",
                "2024-12-31",
                "2024-01-01",
                "Automatic",
                "",
                ""
        );

// Example 2
        EventPackage example2 = new EventPackage(
                "Birthday Package",
                "Celebrate your birthday with our all-inclusive package.",
                "15.0",
                true,
                true,
                "Entertainment",
                "Birthday",
                "3000.0",
                "birthday_party.jpg",
                "2024-10-15",
                "2024-09-30",
                "Manual",
                "",
                ""
        );

// Example 3
        ArrayList<Product> products3 = new ArrayList<>();
        //products3.add(new Product(1L, "Electronics", "Smartphones", "iPhone 12", "Latest iPhone model", "1000", "10%", "900", "image1", "Tech", true, true));
        //products3.add(new Product(2L, "Clothing", "T-shirts", "Cotton T-shirt", "Comfortable cotton T-shirt", "20", "5%", "19", "image2", "Fashion", true, false));

        EventPackage example3 = new EventPackage(
                "Party Package",
                "Get ready to party with our exciting package.",
                "0.0",
                true,
                true,
                "Entertainment",
                "Party",
                "2000.0",
                "party_supplies.jpg",
                "2024-11-30",
                "2024-11-15",
                "Automatic",
                "\n" +
                        "Name: iPhone 12\n" +
                        "Description: Latest iPhone model\n" +
                        "Price: 1000\n" +
                        "Discount: 10%\n" +
                        "Category: Electronics\n" +
                        "Subcategory: Smartphones\n" +
                        "Event Type: Tech" +
                        "\n" +
                        "\n" +
                        "Name: Cotton T-shirt\n" +
                        "Description: Comfortable cotton T-shirt\n" +
                        "Price: 20\n" +
                        "Discount: 5%\n" +
                        "Category: Clothing\n" +
                        "Subcategory: T-shirts\n" +
                        "Event Type: Fashion"
                ,
                ""
        );

// Example 4
        ArrayList<CustomerService> services4 = new ArrayList<>();
        services4.add(new CustomerService(
                "1",
                "Wedding Catering",
                "Premium Wedding Catering Service",
                "Exquisite catering service for weddings",
                "Venue1",
                "1500",
                "2500",
                "6 hours",
                "Discount available for early bookings",
                "Full service with servers included",
                "2024-09-30",
                "2024-09-15",
                "10%",
                "2250",
                "image1",
                "Customizable menu options",
                "Automatic acceptance",
                "Wedding",
                true,
                true
        ));

        EventPackage example4 = new EventPackage(
                "Photography Package",
                "Capture your special moments with our photography package.",
                "0.0",
                true,
                true,
                "Photography",
                "Event",
                "1500.0",
                "photography_package.jpg",
                "2024-12-31",
                "2024-11-01",
                "Manual",
                "", // No products
                "\n" +
                        "Name: Premium Wedding Catering Service\n" +
                        "Description: Exquisite catering service for weddings\n" +
                        "Full Price: 2500\n" +
                        "Duration: 6 hours\n" +
                        "Location: Venue1\n" +
                        "Reservation Deadline: 2024-09-30\n" +
                        "Cancellation Deadline: 2024-09-15\n"
        );


        packages.add(example1);
        packages.add(example2);
        packages.add(example3);
        packages.add(example4);

    }
}