package com.eventplanner.ui.eventPackages;

import android.media.metrics.Event;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.adapters.EventPackageListAdapter;
import com.eventplanner.databinding.FragmentEventPackageListBinding;
import com.eventplanner.model.EventPackage;

import java.util.ArrayList;

public class EventPackageListFragment extends ListFragment {


    private EventPackageListAdapter adapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<EventPackage> mPackages;
    private FragmentEventPackageListBinding binding;

    public static EventPackageListFragment newInstance(ArrayList<EventPackage> packages){
        EventPackageListFragment fragment = new EventPackageListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, packages);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("ShopApp", "onCreate Packages List Fragment");
        if (getArguments() != null) {
            mPackages = getArguments().getParcelableArrayList(ARG_PARAM);
            adapter = new EventPackageListAdapter(getActivity(), mPackages);
            setListAdapter(adapter);
        }
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i("ShopApp", "onCreateView Package List Fragment");
        binding = FragmentEventPackageListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}