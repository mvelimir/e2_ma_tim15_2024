package com.eventplanner.ui.Company;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.eventplanner.R;
import com.eventplanner.model.Company;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class CompanyRegistrationRequestsFragment extends Fragment {
    private ListView listView;
    private EditText searchCompanyName;
    private EditText searchOwnerName;
    private EditText searchOwnerSurname;
    private EditText searchCompanyEmail;
    private EditText searchOwnerEmail;
    private Button searchButton;
    private Button clearButton;
    private CompanyRequestAdapter companyAdapter;
    private List<Company> companyRequests;
    private FirebaseFirestore db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.company_registration_request_fragment, container, false);

        listView = view.findViewById(R.id.list_view);
        searchCompanyName = view.findViewById(R.id.search_company_name);
        searchOwnerName = view.findViewById(R.id.search_owner_name);
        searchOwnerSurname = view.findViewById(R.id.search_owner_surname);
        searchCompanyEmail = view.findViewById(R.id.search_company_email);
        searchOwnerEmail = view.findViewById(R.id.search_owner_email);
        searchButton = view.findViewById(R.id.search_button);
        clearButton = view.findViewById(R.id.clear_button);

        // Initialize Firestore
        db = FirebaseFirestore.getInstance();

        // Initialize the list
        companyRequests = new ArrayList<>();

        // Set up the adapter
        companyAdapter = new CompanyRequestAdapter(getContext(), R.layout.item_company_request, companyRequests);
        listView.setAdapter(companyAdapter);

        // Fetch all data initially
        fetchCompanyRequests(null, null, null, null, null);

        // Set up the search functionality
        searchButton.setOnClickListener(v -> {
            String companyNameQuery = searchCompanyName.getText().toString().trim();
            String ownerNameQuery = searchOwnerName.getText().toString().trim();
            String ownerSurnameQuery = searchOwnerSurname.getText().toString().trim();
            String companyEmailQuery = searchCompanyEmail.getText().toString().trim();
            String ownerEmailQuery = searchOwnerEmail.getText().toString().trim();

            fetchCompanyRequests(companyNameQuery, ownerNameQuery, ownerSurnameQuery, companyEmailQuery, ownerEmailQuery);
        });

        // Set up the clear functionality
        clearButton.setOnClickListener(v -> {
            searchCompanyName.setText("");
            searchOwnerName.setText("");
            searchOwnerSurname.setText("");
            searchCompanyEmail.setText("");
            searchOwnerEmail.setText("");

            // Fetch all data again
            fetchCompanyRequests(null, null, null, null, null);
        });

        return view;
    }

    private void fetchCompanyRequests(String companyNameQuery, String ownerNameQuery, String ownerSurnameQuery, String companyEmailQuery, String ownerEmailQuery) {
        Query firestoreQuery = db.collection("companies").whereEqualTo("registrationConfirmed", false);

        List<Query> queries = new ArrayList<>();

        if (!TextUtils.isEmpty(companyNameQuery)) {
            queries.add(db.collection("companies")
                    .whereGreaterThanOrEqualTo("companyName", companyNameQuery)
                    .whereLessThanOrEqualTo("companyName", companyNameQuery + "\uf8ff"));
        }
        if (!TextUtils.isEmpty(ownerNameQuery)) {
            queries.add(db.collection("companies")
                    .whereGreaterThanOrEqualTo("ownerName", ownerNameQuery)
                    .whereLessThanOrEqualTo("ownerName", ownerNameQuery + "\uf8ff"));
        }
        if (!TextUtils.isEmpty(ownerSurnameQuery)) {
            queries.add(db.collection("companies")
                    .whereGreaterThanOrEqualTo("ownerSurname", ownerSurnameQuery)
                    .whereLessThanOrEqualTo("ownerSurname", ownerSurnameQuery + "\uf8ff"));
        }
        if (!TextUtils.isEmpty(companyEmailQuery)) {
            queries.add(db.collection("companies")
                    .whereGreaterThanOrEqualTo("companyEmail", companyEmailQuery)
                    .whereLessThanOrEqualTo("companyEmail", companyEmailQuery + "\uf8ff"));
        }
        if (!TextUtils.isEmpty(ownerEmailQuery)) {
            queries.add(db.collection("companies")
                    .whereGreaterThanOrEqualTo("ownerMail", ownerEmailQuery)
                    .whereLessThanOrEqualTo("ownerMail", ownerEmailQuery + "\uf8ff"));
        }

        companyRequests.clear();
        int[] count = {0};

        for (Query query : queries) {
            query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Company company = document.toObject(Company.class);
                            if (!companyRequests.contains(company)) {
                                companyRequests.add(company);
                                count[0]++;
                            }
                        }
                        companyAdapter.notifyDataSetChanged();
                        Log.d("CompanyRequests", "Number of companies found: " + count[0]);
                        Toast.makeText(getContext(), "Number of companies found: " + count[0], Toast.LENGTH_SHORT).show();
                    } else {
                        Log.e("CompanyRequests", "Error getting documents: ", task.getException());
                        Toast.makeText(getContext(), "Error getting documents: " + task.getException(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        if (queries.isEmpty()) {
            firestoreQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        companyRequests.clear();
                        int count = 0;
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Company company = document.toObject(Company.class);
                            companyRequests.add(company);
                            count++;
                        }
                        companyAdapter.notifyDataSetChanged();
                        Log.d("CompanyRequests", "Number of companies found: " + count);
                        Toast.makeText(getContext(), "Number of companies found: " + count, Toast.LENGTH_SHORT).show();
                    } else {
                        Log.e("CompanyRequests", "Error getting documents: ", task.getException());
                        Toast.makeText(getContext(), "Error getting documents: " + task.getException(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
