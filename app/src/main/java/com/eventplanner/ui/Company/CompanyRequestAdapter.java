package com.eventplanner.ui.Company;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.eventplanner.R;
import com.eventplanner.model.Company;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class CompanyRequestAdapter extends ArrayAdapter<Company> {
    private Context mContext;
    private int mResource;
    private List<Company> mCompanies;

    public CompanyRequestAdapter(@NonNull Context context, int resource, @NonNull List<Company> companies) {
        super(context, resource, companies);
        this.mContext = context;
        this.mResource = resource;
        this.mCompanies = companies;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(mResource, parent, false);
        }

        Company company = mCompanies.get(position);

        TextView companyInfo = convertView.findViewById(R.id.company_info);
        Button viewDetailsButton = convertView.findViewById(R.id.view_details_button);
        Button approveButton = convertView.findViewById(R.id.approve_button);
        Button rejectButton = convertView.findViewById(R.id.reject_button);

        String info = "Name: " + company.getCompanyName() + "\nEmail: " + company.getCompanyEmail() + "\nAddress: " + company.getCompanyAddress();
        companyInfo.setText(info);

        viewDetailsButton.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putParcelable("company", company);

            NavController navController = Navigation.findNavController((FragmentActivity) mContext, R.id.nav_home);
            navController.navigate(R.id.action_companyRegistrationRequestsFragment_to_companyDetailsFragment, bundle);
        });

        approveButton.setOnClickListener(v -> {
            // Handle approval logic
            approveCompanyRegistration(company);
        });

        rejectButton.setOnClickListener(v -> {
            // Handle rejection logic
            rejectCompanyRegistration(company);
        });

        return convertView;
    }

    private void approveCompanyRegistration(Company company) {
        // Implement approval logic, e.g., updating the Firestore document
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("companies").document(company.getId())
                .update("IsRegistrationConfirmed", true)
                .addOnSuccessListener(aVoid -> {
                    // Send activation email with link (implementation required)
                    sendActivationEmail(company);
                    Toast.makeText(mContext, "Company approved", Toast.LENGTH_SHORT).show();
                })
                .addOnFailureListener(e -> Toast.makeText(mContext, "Error approving company", Toast.LENGTH_SHORT).show());
    }

    private void sendActivationEmail(Company company) {
        // Implement email sending logic with activation link valid for 24 hours
    }

    private void rejectCompanyRegistration(Company company) {
        // Show a dialog to enter the rejection reason
        showRejectionDialog(company);
    }

    private void showRejectionDialog(Company company) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext);
        builder.setTitle("Reject Registration");

        final EditText input = new EditText(mContext);
        input.setHint("Enter reason for rejection");
        builder.setView(input);

        builder.setPositiveButton("Reject", (dialog, which) -> {
            String reason = input.getText().toString().trim();
            if (!reason.isEmpty()) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("companies").document(company.getId())
                        .delete()
                        .addOnSuccessListener(aVoid -> {
                            // Send rejection email with reason (implementation required)
                            sendRejectionEmail(company, reason);
                            Toast.makeText(mContext, "Company rejected", Toast.LENGTH_SHORT).show();
                        })
                        .addOnFailureListener(e -> Toast.makeText(mContext, "Error rejecting company", Toast.LENGTH_SHORT).show());
            } else {
                Toast.makeText(mContext, "Rejection reason cannot be empty", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }

    private void sendRejectionEmail(Company company, String reason) {
        // Implement email sending logic with rejection reason
    }
}
