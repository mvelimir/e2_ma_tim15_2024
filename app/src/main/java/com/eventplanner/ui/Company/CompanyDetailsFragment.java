package com.eventplanner.ui.Company;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.eventplanner.R;
import com.eventplanner.model.Company;

public class CompanyDetailsFragment extends Fragment {

    private TextView companyName;
    private TextView companyEmail;
    private TextView companyAddress;
    private TextView companyPhone;
    private TextView companyDescription;
    private TextView ownerName;
    private TextView ownerSurname;
    private TextView ownerEmail;
    private TextView registrationDate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.company_details_fragment, container, false);

        companyName = view.findViewById(R.id.company_details_name);
        companyEmail = view.findViewById(R.id.company_details_email);
        companyAddress = view.findViewById(R.id.company_details_address);
        companyPhone = view.findViewById(R.id.company_details_phone);
        companyDescription = view.findViewById(R.id.company_details_description);
        ownerName = view.findViewById(R.id.company_details_owner_name);
        ownerSurname = view.findViewById(R.id.company_details_owner_surname);
        ownerEmail = view.findViewById(R.id.company_details_owner_email);
        registrationDate = view.findViewById(R.id.company_details_registration_date);

        if (getArguments() != null) {
            Company company = getArguments().getParcelable("company");
            if (company != null) {
                companyName.setText("Name: " + company.getCompanyName());
                companyEmail.setText("Email: " + company.getCompanyEmail());
                companyAddress.setText("Address: " + company.getCompanyAddress());
                companyPhone.setText("Phone: " + company.getCompanyPhone());
                companyDescription.setText("Description: " + company.getCompanyDescritption());
                ownerName.setText("Owner Name: " + company.getOwnerName());
                ownerSurname.setText("Owner Surname: " + company.getOwnerSurname());
                ownerEmail.setText("Owner Email: " + company.getOwnerMail());
                //registrationDate.setText("Registration Date: " + company.getRegistrationRequestDate().toString());
            }
        }

        return view;
    }
}
