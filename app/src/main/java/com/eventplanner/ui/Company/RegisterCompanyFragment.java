package com.eventplanner.ui.Company;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.eventplanner.R;
import com.eventplanner.databinding.RegisterCompanyFragmentBinding;
import com.eventplanner.model.Company;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;

public class RegisterCompanyFragment extends Fragment {

    private RegisterCompanyFragmentBinding binding;
    private Uri imageUri;
    private String ownerId;


    private ImageButton companyPhotoButton;
    private Button registerCompanyButton;
    private ActivityResultLauncher<Intent> galleryResultLauncher;
    private String ownerName;
    private String ownerSurname;
    private String ownerMail;
    private Bundle bundle = getArguments();

    public RegisterCompanyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = RegisterCompanyFragmentBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        if(getArguments() != null){
            ownerId = getArguments().getString("ownerId");
            ownerName = getArguments().getString("ownerName");
            ownerSurname = getArguments().getString("ownerSurname");
            ownerMail = getArguments().getString("ownerMail");
            Log.d("TAG", "OWNER ID SUCCESSFULLY RETRIEVED");
        }else{
            Log.d("TAG", "ERROR RETRIEVEING OWNER ID");
        }


        galleryResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        imageUri = data.getData();
                        Bitmap bitmapImage = null;
                        try {
                            bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                            // Set the image to an ImageView if you have one
                            // binding.companyImageView.setImageBitmap(bitmapImage);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        companyPhotoButton = binding.companyPhotoButton;
        registerCompanyButton = binding.registerCompanyButton;

        companyPhotoButton.setOnClickListener(view1 -> {
            if (ContextCompat.checkSelfPermission(requireActivity(),
                    Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
            } else {
                startGallery();
            }
        });

        registerCompanyButton.setOnClickListener(view12 -> registerCompany());
    }

    private void startGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        galleryResultLauncher.launch(intent);
    }

    @SuppressLint("Range")
    private String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private void registerCompany() {
        String email = binding.companyEmail.getText().toString();
        String name = binding.companyName.getText().toString();
        String address = binding.companyAddress.getText().toString();
        String phone = binding.companyPhone.getText().toString();
        String description = binding.companyDescription.getText().toString();

        if (email.isEmpty() || name.isEmpty() || address.isEmpty() || phone.isEmpty() || description.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill out all fields", Toast.LENGTH_SHORT).show();
            return;
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();

        String image = "";
        if (imageUri != null) {
            image = "images/" + getFileName(imageUri);
            StorageReference companyImageRef = storageRef.child(image);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();

                companyImageRef.putBytes(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        DocumentReference companyRef = db.collection("companies").document();

        Company company = new Company(companyRef.getId(), email, name, address, phone, description, image, ownerId, ownerName, ownerSurname, ownerMail, false,new Date());
        companyRef.set(company).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getActivity(), "Company successfully registered", Toast.LENGTH_SHORT).show();
                    // Navigate to the next screen if needed
                    //Navigation.findNavController(binding.getRoot()).navigate(R.id.action_registerCompanyFragment_to_nextFragment);
                } else {
                    Toast.makeText(getActivity(), "Company registration failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
