package com.eventplanner.ui.companies;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentCompanyReviewReportsBinding;
import com.eventplanner.databinding.FragmentOwnerProfileBinding;
import com.eventplanner.model.CompanyReviewReport;
import com.eventplanner.services.CompanyReviewReportService;

import java.util.ArrayList;

public class CompanyReviewReportsFragment extends Fragment {

    private FragmentCompanyReviewReportsBinding binding;
    private ArrayList<CompanyReviewReport> reports;

    public CompanyReviewReportsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCompanyReviewReportsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        CompanyReviewReportService service = new CompanyReviewReportService();

        service.getAll(rs -> {
            reports = rs;

            FragmentTransition.to(CompanyReviewReportListFragment.newInstance(reports), getActivity(),
                    false, R.id.scroll_company_review_reports_list);
        });

        return root;
    }
}