package com.eventplanner.ui.companies;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.eventplanner.adapters.CompanyReviewListAdapter;
import com.eventplanner.databinding.FragmentCompanyReviewListBinding;
import com.eventplanner.model.CompanyReview;

import java.util.ArrayList;

public class CompanyReviewListFragment extends ListFragment {
    private CompanyReviewListAdapter adapter;
    private ArrayList<CompanyReview> reviews;
    private FragmentCompanyReviewListBinding binding;

    public static CompanyReviewListFragment newInstance(ArrayList<CompanyReview> reviews){
        CompanyReviewListFragment fragment = new CompanyReviewListFragment();
        fragment.reviews = reviews;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new CompanyReviewListAdapter(getActivity(), reviews);
        setListAdapter(adapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCompanyReviewListBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
