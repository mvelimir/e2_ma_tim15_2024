package com.eventplanner.ui.companies;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentCompanyBinding;
import com.eventplanner.databinding.FragmentReservationBinding;
import com.eventplanner.model.CompanyReview;
import com.eventplanner.model.Owner;
import com.eventplanner.model.Reservation;
import com.eventplanner.services.CompanyReviewService;
import com.eventplanner.services.UserService;
import com.eventplanner.ui.reservations.ReservationListFragment;

import java.util.ArrayList;

public class CompanyFragment extends Fragment {
    private String companyId;

    public String getCompanyId() {
        return companyId;
    }

    public static ArrayList<CompanyReview> reviews;

    private FragmentCompanyBinding binding;


    public CompanyFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            companyId = getArguments().getString("companyId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCompanyBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        CompanyReviewService service = new CompanyReviewService();
        UserService userService = new UserService();

        if (companyId == null) {
            userService.getUser(u -> {
                userService.get(u.getId(), Owner.class, o -> {
                    companyId = o.getCompanyId();

                    service.getAllForCompany(companyId, rs -> {
                        reviews = rs;

                        FragmentTransition.to(CompanyReviewListFragment.newInstance(reviews), getActivity(),
                                false, R.id.scroll_company_reviews_list);
                    });
                });
            });
        } else {

            service.getAllForCompany(companyId, rs -> {
                reviews = rs;

                FragmentTransition.to(CompanyReviewListFragment.newInstance(reviews), getActivity(),
                        false, R.id.scroll_company_reviews_list);
            });
        }

        return root;
    }
}