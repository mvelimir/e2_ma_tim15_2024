package com.eventplanner.ui.companies;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.eventplanner.R;
import com.eventplanner.adapters.CompanyReviewListAdapter;
import com.eventplanner.adapters.CompanyReviewReportListAdapter;
import com.eventplanner.databinding.FragmentCompanyReviewListBinding;
import com.eventplanner.databinding.FragmentCompanyReviewReportListBinding;
import com.eventplanner.model.CompanyReview;
import com.eventplanner.model.CompanyReviewReport;

import java.util.ArrayList;

public class CompanyReviewReportListFragment extends ListFragment {
    private CompanyReviewReportListAdapter adapter;
    private ArrayList<CompanyReviewReport> reports;
    private FragmentCompanyReviewReportListBinding binding;

    private NavController navController;

    public static CompanyReviewReportListFragment newInstance(ArrayList<CompanyReviewReport> reports){
        CompanyReviewReportListFragment fragment = new CompanyReviewReportListFragment();
        fragment.reports = reports;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new CompanyReviewReportListAdapter(getActivity(), reports);
        setListAdapter(adapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCompanyReviewReportListBinding.inflate(inflater, container, false);

        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment_content_main);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
