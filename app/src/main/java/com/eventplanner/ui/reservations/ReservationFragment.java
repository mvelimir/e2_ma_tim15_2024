package com.eventplanner.ui.reservations;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeesBinding;
import com.eventplanner.databinding.FragmentReservationBinding;
import com.eventplanner.model.Employee;
import com.eventplanner.services.CustomerServiceService;
import com.eventplanner.services.ReservationService;
import com.eventplanner.services.UserService;
import com.google.android.material.button.MaterialButton;

import java.text.SimpleDateFormat;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReservationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReservationFragment extends Fragment {
    private FragmentReservationBinding binding;
    private ReservationService service;
    private UserService userService;
    private CustomerServiceService customerServiceService;
    private String reservationId;

    private NavController navController;


    public ReservationFragment() {
        // Required empty public constructor
    }

    public static ReservationFragment newInstance() {
        return new ReservationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            reservationId = getArguments().getString("reservationId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentReservationBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        MaterialButton companyDetailsButton = binding.companyDetailsButton;
        MaterialButton packageDetailsButton = binding.packageDetailsButton;

        service = new ReservationService();
        userService = new UserService();
        customerServiceService = new CustomerServiceService();

        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment_content_main);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat fullDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

        TextView serviceName = binding.serviceName;
        TextView reservationStatus = binding.reservationStatus;
        TextView serviceDetails = binding.serviceDetails;
        TextView eventDetails = binding.eventDetails;
        TextView eventDate = binding.eventDate;
        TextView eventStartTime = binding.eventStartTime;
        TextView eventEndTime = binding.eventEndTime;
        TextView organizedBy = binding.organizedBy;
        TextView forEmployee = binding.forEmployee;
        TextView withCancellationUntil = binding.withCancellationUntil;

        service.get(reservationId, r -> {
            userService.get(r.getEmployeeId(), Employee.class, e -> {
                companyDetailsButton.setOnClickListener(v -> {
                    String companyId = e.getCompanyId();

                    Bundle bundle = new Bundle();
                    bundle.putString("companyId", companyId);

                    navController.navigate(R.id.nav_company, bundle);
                });
            });

            customerServiceService.get(r.getServiceId(), s -> {
                serviceDetails.setText("With service price of " + s.getFullPrice().toString() + " at " + s.getLocation());
                eventDetails.setText("with duration of" + s.getDuration());
                serviceName.setText(s.getName());
            });
            reservationStatus.setText(r.getStatus().toString().replace("_", " "));
            withCancellationUntil.setText("With deadline at " + fullDateFormat.format(r.getCancellationTimeLimit()));

            eventDate.setText(dateFormat.format(r.getFrom()));
            eventStartTime.setText(timeFormat.format(r.getFrom()));
            eventEndTime.setText(timeFormat.format(r.getTo()));

            if (!r.isPartOfPackage()) {
                packageDetailsButton.setVisibility(View.GONE);
            }

            if (r.getEmployeeId() != null) {
                userService.get(r.getEmployeeId(), u -> {
                    forEmployee.setText("For " + u.getFirstName() + " " + u.getLastName());
                });
            }
            if (r.getOrganizerId() != null) {
                userService.get(r.getOrganizerId(), u -> {
                    organizedBy.setText("Organized by " + u.getFirstName() + " " + u.getLastName());
                });
            }
        });

        return root;
    }
}