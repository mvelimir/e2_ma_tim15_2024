package com.eventplanner.ui.reservations;

import static android.view.View.GONE;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;

import com.eventplanner.FragmentTransition;
import com.eventplanner.R;
import com.eventplanner.databinding.FragmentEmployeesBinding;
import com.eventplanner.databinding.FragmentReservationsBinding;
import com.eventplanner.model.Employee;
import com.eventplanner.model.Owner;
import com.eventplanner.model.Reservation;
import com.eventplanner.model.ReservationStatus;
import com.eventplanner.model.User;
import com.eventplanner.services.ReservationService;
import com.eventplanner.services.UserService;
import com.eventplanner.ui.employees.EmployeeListFragment;
import com.eventplanner.ui.employees.EmployeesViewModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

public class ReservationsFragment extends Fragment {
    public static ArrayList<Reservation> reservations;
    public static ArrayList<Reservation> filteredReservations;
    private FragmentReservationsBinding binding;
    private SearchView search;
    private Spinner searchOption;
    private Spinner filterOption;
    private LinearLayout searchLayout;

    private class ReservationComparator implements Comparator<Reservation> {
        @Override
        public int compare(Reservation r1, Reservation r2) {
            return r2.getFrom().compareTo(r1.getFrom());
        }
    }

    private ReservationService service;
    private UserService userService;

    public ReservationsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentReservationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        reservations = new ArrayList<>();
        filteredReservations = new ArrayList<>();

        search = binding.search;
        searchOption = binding.searchOption;
        filterOption = binding.filterOption;
        searchLayout = binding.searchLayout;

        service = new ReservationService();
        userService = new UserService();

        userService.getUser(u -> {
            switch (u.getType()) {
                case ORGANIZER:
                    searchLayout.setVisibility(GONE);
                    break;
                case EMPLOYEE:
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                            R.array.search_by_options_for_reservations_for_employee, android.R.layout.simple_spinner_item);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    searchOption.setAdapter(adapter);

                    break;
            }
        });

        filterOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setFilteredReservations();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.isEmpty()) {
                    service.getAll(rs -> updateReservations(rs));

                    return true;
                }

                switch (searchOption.getSelectedItem().toString()) {
                    case "Organizer full name":
                        if (query.split(" ").length != 2) {
                            reservations = new ArrayList<>();
                            return true;
                        }
                        service.getByOrganizerFullName(query, rs -> updateReservations(rs));
                        break;
                    case "Employee full name":
                        if (query.split(" ").length != 2) {
                            reservations = new ArrayList<>();
                            return true;
                        }
                        service.getByEmployeeFullName(query, rs -> updateReservations(rs));
                        break;
                    case "Service name":
                        service.getByServiceName(query, rs -> updateReservations(rs));
                        break;
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.isEmpty()){
                    this.onQueryTextSubmit("");
                }

                return true;
            }
        });

        service.getAll(this::updateReservations);

        return root;
    }

    private void updateReservations(ArrayList<Reservation> reservations) {
        ReservationsFragment.reservations = reservations;
        ReservationsFragment.reservations.sort(new ReservationComparator());
        removeForUserRole();
        setFilteredReservations();
    }

    private void removeForUserRole() {
        userService.getUser().addOnSuccessListener(queryDocumentSnapshots -> {
            User user = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class);

            switch (user.getType()) {
                case EMPLOYEE:
                    reservations = reservations.stream().filter(r -> r.getEmployeeId().equals(user.getId())).collect(Collectors.toCollection(ArrayList::new));
                    break;
                case ORGANIZER:
                    reservations = reservations.stream().filter(r -> r.getOrganizerId().equals(user.getId())).collect(Collectors.toCollection(ArrayList::new));
                    break;
            }
        });
    }

    private void setFilteredReservations() {
        switch (filterOption.getSelectedItem().toString()) {
            case "All":
                filteredReservations = reservations;
                break;
            case "Cancelled by employee":
                filteredReservations = reservations.stream().filter(r -> r.getStatus() == ReservationStatus.CANCELLED_BY_EMPLOYEE).collect(Collectors.toCollection(ArrayList::new));
                break;
            case "Cancelled by organizer":
                filteredReservations = reservations.stream().filter(r -> r.getStatus() == ReservationStatus.CANCELLED_BY_ORGANIZER).collect(Collectors.toCollection(ArrayList::new));
                break;
            case "Cancelled by admin":
                filteredReservations = reservations.stream().filter(r -> r.getStatus() == ReservationStatus.CANCELLED_BY_ADMIN).collect(Collectors.toCollection(ArrayList::new));
                break;
            case "Accepted":
                filteredReservations = reservations.stream().filter(r -> r.getStatus() == ReservationStatus.ACCEPTED).collect(Collectors.toCollection(ArrayList::new));
            case "Realized":
                filteredReservations = reservations.stream().filter(r -> r.getStatus() == ReservationStatus.REALIZED).collect(Collectors.toCollection(ArrayList::new));
                break;
        }

        FragmentTransition.to(ReservationListFragment.newInstance(filteredReservations), getActivity(),
                false, R.id.scroll_reservations_list);
    }
}