package com.eventplanner.ui.reservations;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eventplanner.R;
import com.eventplanner.adapters.ReservationListAdapter;
import com.eventplanner.databinding.FragmentEmployeeListBinding;
import com.eventplanner.databinding.FragmentReservationListBinding;
import com.eventplanner.model.Employee;
import com.eventplanner.model.Reservation;

import java.util.ArrayList;

public class ReservationListFragment extends ListFragment {
    private ReservationListAdapter adapter;
    private ArrayList<Reservation> reservations;
    private FragmentReservationListBinding binding;

    public static ReservationListFragment newInstance(ArrayList<Reservation> reservations){
        ReservationListFragment fragment = new ReservationListFragment();
        fragment.reservations = reservations;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ReservationListAdapter(getActivity(), reservations);
        setListAdapter(adapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentReservationListBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}