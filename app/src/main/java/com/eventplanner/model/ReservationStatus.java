package com.eventplanner.model;

public enum ReservationStatus {
    NEW,
    CANCELLED_BY_EMPLOYEE,
    CANCELLED_BY_ORGANIZER,
    CANCELLED_BY_ADMIN,
    ACCEPTED,
    REALIZED,
}
