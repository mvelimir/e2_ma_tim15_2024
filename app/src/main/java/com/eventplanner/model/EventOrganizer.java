package com.eventplanner.model;

import android.net.Uri;

public class EventOrganizer extends User{
    private String id;

    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNumber;

    private String image;
    private UserType type;
    private boolean IsRegistrationConfirmed;

    public EventOrganizer(String id, String email, String password, String firstName, String lastName, String address, String phoneNumber, String image, UserType type, boolean IsRegistrationConfirmed) {
        super(id, email, password, firstName, lastName, address, phoneNumber, image, type, IsRegistrationConfirmed);

        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.IsRegistrationConfirmed = false;
        this.type = UserType.EVENT_ORGANIZER;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setRegistrationConfirmed(boolean registrationConfirmed) {
        IsRegistrationConfirmed = registrationConfirmed;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public boolean getIsRegistrationConfirmed(){
        return IsRegistrationConfirmed;
    }

    public String getImage() {
        return image;
    }

    public UserType getType() {
        return type;
    }

    public EventOrganizer(){super ();}
}
