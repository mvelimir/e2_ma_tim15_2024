package com.eventplanner.model;

import java.util.Date;

public class EmployeeSchedule {
    private String id;
    private Date from;
    private Date to;
    private Date mondayStart;
    private Date mondayEnd;
    private Date tuesdayStart;
    private Date tuesdayEnd;
    private Date wednesdayStart;
    private Date wednesdayEnd;
    private Date thursdayStart;
    private Date thursdayEnd;
    private Date fridayStart;
    private Date fridayEnd;
    private Date saturdayStart;
    private Date saturdayEnd;
    private Date sundayStart;
    private Date sundayEnd;

    public EmployeeSchedule() {
    }

    public EmployeeSchedule(String id, Date from, Date to, Date mondayStart, Date mondayEnd, Date tuesdayStart, Date tuesdayEnd, Date wednesdayStart, Date wednesdayEnd, Date thursdayStart, Date thursdayEnd, Date fridayStart, Date fridayEnd, Date saturdayStart, Date saturdayEnd, Date sundayStart, Date sundayEnd) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.mondayStart = mondayStart;
        this.mondayEnd = mondayEnd;
        this.tuesdayStart = tuesdayStart;
        this.tuesdayEnd = tuesdayEnd;
        this.wednesdayStart = wednesdayStart;
        this.wednesdayEnd = wednesdayEnd;
        this.thursdayStart = thursdayStart;
        this.thursdayEnd = thursdayEnd;
        this.fridayStart = fridayStart;
        this.fridayEnd = fridayEnd;
        this.saturdayStart = saturdayStart;
        this.saturdayEnd = saturdayEnd;
        this.sundayStart = sundayStart;
        this.sundayEnd = sundayEnd;
    }

    public EmployeeSchedule(String id, Date from, Date mondayStart, Date mondayEnd, Date tuesdayStart, Date tuesdayEnd, Date wednesdayStart, Date wednesdayEnd, Date thursdayStart, Date thursdayEnd, Date fridayStart, Date fridayEnd, Date saturdayStart, Date saturdayEnd, Date sundayStart, Date sundayEnd) {
        this.id = id;
        this.from = from;
        this.mondayStart = mondayStart;
        this.mondayEnd = mondayEnd;
        this.tuesdayStart = tuesdayStart;
        this.tuesdayEnd = tuesdayEnd;
        this.wednesdayStart = wednesdayStart;
        this.wednesdayEnd = wednesdayEnd;
        this.thursdayStart = thursdayStart;
        this.thursdayEnd = thursdayEnd;
        this.fridayStart = fridayStart;
        this.fridayEnd = fridayEnd;
        this.saturdayStart = saturdayStart;
        this.saturdayEnd = saturdayEnd;
        this.sundayStart = sundayStart;
        this.sundayEnd = sundayEnd;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Date getMondayStart() {
        return mondayStart;
    }

    public void setMondayStart(Date mondayStart) {
        this.mondayStart = mondayStart;
    }

    public Date getMondayEnd() {
        return mondayEnd;
    }

    public void setMondayEnd(Date mondayEnd) {
        this.mondayEnd = mondayEnd;
    }

    public Date getTuesdayStart() {
        return tuesdayStart;
    }

    public void setTuesdayStart(Date tuesdayStart) {
        this.tuesdayStart = tuesdayStart;
    }

    public Date getTuesdayEnd() {
        return tuesdayEnd;
    }

    public void setTuesdayEnd(Date tuesdayEnd) {
        this.tuesdayEnd = tuesdayEnd;
    }

    public Date getWednesdayStart() {
        return wednesdayStart;
    }

    public void setWednesdayStart(Date wednesdayStart) {
        this.wednesdayStart = wednesdayStart;
    }

    public Date getWednesdayEnd() {
        return wednesdayEnd;
    }

    public void setWednesdayEnd(Date wednesdayEnd) {
        this.wednesdayEnd = wednesdayEnd;
    }

    public Date getThursdayStart() {
        return thursdayStart;
    }

    public void setThursdayStart(Date thursdayStart) {
        this.thursdayStart = thursdayStart;
    }

    public Date getThursdayEnd() {
        return thursdayEnd;
    }

    public void setThursdayEnd(Date thursdayEnd) {
        this.thursdayEnd = thursdayEnd;
    }

    public Date getFridayStart() {
        return fridayStart;
    }

    public void setFridayStart(Date fridayStart) {
        this.fridayStart = fridayStart;
    }

    public Date getFridayEnd() {
        return fridayEnd;
    }

    public void setFridayEnd(Date fridayEnd) {
        this.fridayEnd = fridayEnd;
    }

    public Date getSaturdayStart() {
        return saturdayStart;
    }

    public void setSaturdayStart(Date saturdayStart) {
        this.saturdayStart = saturdayStart;
    }

    public Date getSaturdayEnd() {
        return saturdayEnd;
    }

    public void setSaturdayEnd(Date saturdayEnd) {
        this.saturdayEnd = saturdayEnd;
    }

    public Date getSundayStart() {
        return sundayStart;
    }

    public void setSundayStart(Date sundayStart) {
        this.sundayStart = sundayStart;
    }

    public Date getSundayEnd() {
        return sundayEnd;
    }

    public void setSundayEnd(Date sundayEnd) {
        this.sundayEnd = sundayEnd;
    }
}
