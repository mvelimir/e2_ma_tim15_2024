package com.eventplanner.model;

public enum CompanyReviewReportStatus {
    REPORTED,
    ACCEPTED,
    REJECTED
}
