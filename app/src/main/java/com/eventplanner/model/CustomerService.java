package com.eventplanner.model;

import android.os.Parcelable;
import android.os.Parcel;
import com.eventplanner.model.CustomerService;


public class CustomerService implements Parcelable {

    public enum AcceptanceType {
        AUTOMATIC, MANUAL
    }
    private String id;
    private String category;
    private String subcategory;
    private String name;
    private String description;
    private String priceByAnHour;
    private String fullPrice;
    private String duration;
    private String location;
    private String discount;

    private String servers;
    private String reservationDeadline;
    private String cancellationDeadline;
    private String discountedPrice;
    private String gallery;
    private String specifics;

    private String acceptanceType;
    private String eventType;
    private boolean available;
    private boolean visible;

    public CustomerService() {

    }

    public CustomerService(String id, String category, String subcategory, String name, String description, String priceByAnHour, String fullPrice, String duration, String location, String discount, String servers, String reservationDeadline, String cancellationDeadline, String discountedPrice, String gallery, String specifics, String acceptanceType, String eventType, boolean available, boolean visible) {
        this.id = id;
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
        this.description = description;
        this.priceByAnHour = priceByAnHour;
        this.fullPrice = fullPrice;
        this.duration = duration;
        this.location = location;
        this.discount = discount;
        this.servers = servers;
        this.reservationDeadline = reservationDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.discountedPrice = discountedPrice;
        this.gallery = gallery;
        this.specifics = specifics;
        this.acceptanceType = acceptanceType;
        this.eventType = eventType;
        this.available = available;
        this.visible = visible;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriceByAnHour() {
        return priceByAnHour;
    }

    public void setPriceByAnHour(String priceByAnHour) {
        this.priceByAnHour = priceByAnHour;
    }

    public String getFullPrice() {
        return fullPrice;
    }

    public void setFullPrice(String fullPrice) {
        this.fullPrice = fullPrice;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getServers() {
        return servers;
    }

    public void setServers(String servers) {
        this.servers = servers;
    }

    public String getReservationDeadline() {
        return reservationDeadline;
    }

    public void setReservationDeadline(String reservationDeadline) {
        this.reservationDeadline = reservationDeadline;
    }

    public String getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(String cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public String getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(String discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public String getGallery() {
        return gallery;
    }

    public void setGallery(String gallery) {
        this.gallery = gallery;
    }

    public String getSpecifics() {
        return specifics;
    }

    public void setSpecifics(String specifics) {
        this.specifics = specifics;
    }

    public String getAcceptanceType() {
        return acceptanceType;
    }

    public void setAcceptanceType(String acceptanceType) {
        this.acceptanceType = acceptanceType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    // Parcelable implementation

    protected CustomerService(Parcel in) {
        id = in.readString();
        category = in.readString();
        subcategory = in.readString();
        name = in.readString();
        description = in.readString();
        priceByAnHour = in.readString();
        fullPrice = in.readString();
        duration = in.readString();
        location = in.readString();
        discount = in.readString();
        servers = in.readString();
        reservationDeadline = in.readString();
        cancellationDeadline = in.readString();
        discountedPrice = in.readString();
        gallery = in.readString();
        specifics = in.readString();
        acceptanceType = in.readString();
        eventType = in.readString();
        available = in.readByte() != 0;
        visible = in.readByte() != 0;
    }

    public static final Creator<CustomerService> CREATOR = new Creator<CustomerService>() {
        @Override
        public CustomerService createFromParcel(Parcel in) {
            return new CustomerService(in);
        }

        @Override
        public CustomerService[] newArray(int size) {
            return new CustomerService[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(category);
        dest.writeString(subcategory);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(priceByAnHour);
        dest.writeString(fullPrice);
        dest.writeString(duration);
        dest.writeString(location);
        dest.writeString(discount);
        dest.writeString(servers);
        dest.writeString(reservationDeadline);
        dest.writeString(cancellationDeadline);
        dest.writeString(discountedPrice);
        dest.writeString(gallery);
        dest.writeString(specifics);
        dest.writeString(acceptanceType);
        dest.writeString(eventType);
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeByte((byte) (visible ? 1 : 0));
    }
}