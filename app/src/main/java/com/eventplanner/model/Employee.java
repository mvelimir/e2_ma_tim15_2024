package com.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Employee extends User implements Parcelable {
    private String companyId;
    private boolean isBlocked;

    public Employee() {
        super();
        type = UserType.EMPLOYEE;
    }

    public Employee(String id, String email, String password, String firstName, String lastName, String homeAddress, String phoneNumber, String image, String companyId, boolean isBlocked) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.homeAddress = homeAddress;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.isBlocked = isBlocked;
        this.companyId = companyId;
        this.type = UserType.EMPLOYEE;
    }

    protected Employee(Parcel in) {
        id = in.readString();
        email = in.readString();
        password = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        homeAddress = in.readString();
        phoneNumber = in.readString();
        image = in.readString();
        companyId = in.readString();
        isBlocked = in.readBoolean();
        type = UserType.EMPLOYEE;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(homeAddress);
        dest.writeString(phoneNumber);
        dest.writeString(image);
        dest.writeString(companyId);
        dest.writeBoolean(isBlocked);
    }

    public static final Creator<Employee> CREATOR = new Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel in) {
            return new Employee(in);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };

}
