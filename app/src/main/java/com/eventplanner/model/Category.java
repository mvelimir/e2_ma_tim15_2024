package com.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Objects;

public class Category implements Parcelable {
        private String id;
        private String name;
        private String description;

    public Category() {}

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getId() {
            return id;
        }

    public Category(String id, String name, String description) {
            this.id = id;
            this.name = name;
            this.description = description;
        }

    public Category(String id) {
            this.id = id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setDescription(String description) {
            this.description = description;
        }


    protected Category(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readString();
            }
            name = in.readString();
            description = in.readString();
        }

        public static final Creator<Category> CREATOR = new Creator<Category>() {
            @Override
            public Category createFromParcel(Parcel in) {
                return new Category(in);
            }

            @Override
            public Category[] newArray(int size) {
                return new Category[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(@NonNull Parcel dest, int flags) {
            if (id == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeString(id);
            }
            dest.writeString(name);
            dest.writeString(description);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Category category = (Category) o;
            return Objects.equals(id, category.id) &&
                    Objects.equals(name, category.name) &&
                    Objects.equals(description, category.description);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, name, description);
        }

    }
