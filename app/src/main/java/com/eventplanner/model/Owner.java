package com.eventplanner.model;

import java.util.Date;

public class Owner extends User {
   // private String companyId;
    private String id;

    private String companyId;
    private String email;
    private String name;
    private String password;
    private String surname;
    private String address;
    private String phoneNumber;
    private String image;

    private boolean IsRegistrationConfirmed;
    private UserType type;

    private Date registrationDate;

    @Override
    public Date getRegistrationDate() {
        return registrationDate;
    }


    public Owner() {
        super();
        type = UserType.OWNER;
    }

    public Owner(String id, String email, String password, String firstName, String lastName, String homeAddress, String phoneNumber, String image/*, String companyId*/, UserType type, boolean isRegistrationConfirmed, Date registrationDate) {
        super(id, email, password, firstName, lastName, homeAddress, phoneNumber, image, type, isRegistrationConfirmed);
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = firstName;
        this.surname = lastName;
        this.address = homeAddress;
        this.phoneNumber = phoneNumber;
        this.image = image;
        //this.companyId = companyId;
        this.type = UserType.OWNER;
        this.IsRegistrationConfirmed = false;
        this.registrationDate = registrationDate;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public void setImage(String image) {
        this.image = image;
    }

    public boolean isRegistrationConfirmed() {
        return IsRegistrationConfirmed;
    }

    public void setRegistrationConfirmed(boolean registrationConfirmed) {
        IsRegistrationConfirmed = registrationConfirmed;
    }

    @Override
    public UserType getType() {
        return type;
    }

    @Override
    public void setType(UserType type) {
        this.type = type;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
