package com.eventplanner.model;

import android.os.Parcelable;
import android.os.Parcel;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class EventPackage implements Parcelable {

    private String id;
    private String name;
    private String description;
    private String discount;
    private boolean visibility;
    private boolean availability;
    private String category;
    private String eventType;
    private String price;
    private String images;
    private String reservationDeadline;
    private String cancellationDeadline;
    private String acceptanceType;
//    @Nullable
//    private ArrayList<Product> products;
//    @Nullable
//    private ArrayList<CustomerService> customerServices;
    private String products;
    private String customerServices;

    // Constructor
    public EventPackage(String name, String description, String discount, boolean visibility, boolean availability,
                        String category, String eventType, String price, String images, String reservationDeadline,
                        String cancellationDeadline, String acceptanceType, String products,
                        String customerServices) {
        this.name = name;
        this.description = description;
        this.discount = discount;
        this.visibility = visibility;
        this.availability = availability;
        this.category = category;
        this.eventType = eventType;
        this.price = price;
        this.images = images;
        this.reservationDeadline = reservationDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.acceptanceType = acceptanceType;
        this.products = products;
        this.customerServices = customerServices;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getReservationDeadline() {
        return reservationDeadline;
    }

    public void setReservationDeadline(String reservationDeadline) {
        this.reservationDeadline = reservationDeadline;
    }

    public String getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(String cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public String getAcceptanceType() {
        return acceptanceType;
    }

    public void setAcceptanceType(String acceptanceType) {
        this.acceptanceType = acceptanceType;
    }

    @Nullable
    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    @Nullable
    public String getCustomerServices() {
        return customerServices;
    }

    public void setCustomerServices(String customerServices) {
        this.customerServices = customerServices;
    }

    protected EventPackage(Parcel in) {
        name = in.readString();
        description = in.readString();
        discount = in.readString();
        visibility = in.readByte() != 0;
        availability = in.readByte() != 0;
        category = in.readString();
        eventType = in.readString();
        price = in.readString();
        images = in.readString();
        reservationDeadline = in.readString();
        cancellationDeadline = in.readString();
        acceptanceType = in.readString();
        products = in.readString();
        customerServices = in.readString();
    }

    public static final Parcelable.Creator<EventPackage> CREATOR = new Parcelable.Creator<EventPackage>() {
        @Override
        public EventPackage createFromParcel(Parcel in) {
            return new EventPackage(in);
        }

        @Override
        public EventPackage[] newArray(int size) {
            return new EventPackage[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(discount);
        dest.writeByte((byte) (visibility ? 1 : 0));
        dest.writeByte((byte) (availability ? 1 : 0));
        dest.writeString(category);
        dest.writeString(eventType);
        dest.writeString(price);
        dest.writeString(images);
        dest.writeString(reservationDeadline);
        dest.writeString(cancellationDeadline);
        dest.writeString(acceptanceType);
        dest.writeString(products);
        dest.writeString(customerServices);
    }

}
