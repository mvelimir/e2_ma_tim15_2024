package com.eventplanner.model;

import android.os.Parcelable;
import android.os.Parcel;

import java.util.List;


public class Product implements Parcelable {
    private Long id;
    private List<String> category;
    private List<String> subcategory;
    private String name;
    private String description;
    private String price;
    private String discount;
    private String discountedPrice;
    private String gallery;
    private String eventType;
    private boolean available;
    private boolean visible;

    public Product(Long id, List<String> category, List<String> subcategory, String name, String description, String price, String discount, String discountedPrice, String gallery, String eventType, boolean available, boolean visible) {
        this.id = id;
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.discountedPrice = discountedPrice;
        this.gallery = gallery;
        this.eventType = eventType;
        this.available = available;
        this.visible = visible;
    }

    /*public Product(String name, List<String> category, List<String> subcategory){
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }

    public List<String> getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(List<String> subcategory) {
        this.subcategory = subcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(String discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public String getGallery() {
        return gallery;
    }

    public void setGallery(String gallery) {
        this.gallery = gallery;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }


    // Parcelable implementation

    protected Product(Parcel in) {
        id = in.readLong();
        category = in.createStringArrayList();
        subcategory = in.createStringArrayList();
        name = in.readString();
        description = in.readString();
        price = in.readString();
        discount = in.readString();
        discountedPrice = in.readString();
        gallery = in.readString();
        eventType = in.readString();
        available = in.readByte() != 0;
        visible = in.readByte() != 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeStringList(category);
        dest.writeStringList(subcategory);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(price);
        dest.writeString(discount);
        dest.writeString(discountedPrice);
        dest.writeString(gallery);
        dest.writeString(eventType);
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeByte((byte) (visible ? 1 : 0));
    }
}