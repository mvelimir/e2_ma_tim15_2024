package com.eventplanner.model.event;

public class SpentSubcategoryBudget {
    //jedno od ova dva je null
    private String serviceId;
    private String productId;
    private float cost;

    public SpentSubcategoryBudget() {
    }

    public SpentSubcategoryBudget(String serviceId, String productId, float cost) {
        this.serviceId = serviceId;
        this.productId = productId;
        this.cost = cost;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }
}
