package com.eventplanner.model.event;

import java.util.ArrayList;

public class PlannedSubcategoryBudget {
    private String categoryId;
    private String subcategoryId;
    private float plannedBudget;
    private ArrayList<SpentSubcategoryBudget> spentSubcategoryBudgets;

    public PlannedSubcategoryBudget() {
    }

    public PlannedSubcategoryBudget(String categoryId, String subcategoryId, float plannedBudget, ArrayList<SpentSubcategoryBudget> spentSubcategoryBudgets) {
        this.categoryId = categoryId;
        this.subcategoryId = subcategoryId;
        this.plannedBudget = plannedBudget;
        this.spentSubcategoryBudgets = spentSubcategoryBudgets;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public float getPlannedBudget() {
        return plannedBudget;
    }

    public void setPlannedBudget(float plannedBudget) {
        this.plannedBudget = plannedBudget;
    }

    public ArrayList<SpentSubcategoryBudget> getSpentSubcategoryBudgets() {
        return spentSubcategoryBudgets;
    }

    public void setSpentSubcategoryBudgets(ArrayList<SpentSubcategoryBudget> spentSubcategoryBudgets) {
        this.spentSubcategoryBudgets = spentSubcategoryBudgets;
    }
}
