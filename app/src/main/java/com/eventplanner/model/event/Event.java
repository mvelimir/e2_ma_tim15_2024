package com.eventplanner.model.event;

import java.util.Date;

public class Event {
    private String id;
    private String name;
    private String description;
    private int participantCount;
    private String location;
    private Date date;
    private boolean privateEvent;
    private EventBudget budget;

    public Event() {
    }

    public Event(String id, String name, String description, int participantCount, String location, Date date, boolean privateEvent, EventBudget budget) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.participantCount = participantCount;
        this.location = location;
        this.date = date;
        this.privateEvent = privateEvent;
        this.budget = budget;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getParticipantCount() {
        return participantCount;
    }

    public void setParticipantCount(int participantCount) {
        this.participantCount = participantCount;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isPrivateEvent() {
        return privateEvent;
    }

    public void setPrivateEvent(boolean privateEvent) {
        this.privateEvent = privateEvent;
    }

    public EventBudget getBudget() {
        return budget;
    }

    public void setBudget(EventBudget budget) {
        this.budget = budget;
    }
}
