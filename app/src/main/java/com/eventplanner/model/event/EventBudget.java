package com.eventplanner.model.event;

import android.util.Log;

import java.util.ArrayList;

public class EventBudget {
    private ArrayList<PlannedSubcategoryBudget> plannedSubcategoryBudgets;
    private float totalPlannedBudget;
    private float totalCost;

    public EventBudget() {
    }

    public EventBudget(ArrayList<PlannedSubcategoryBudget> plannedSubcategoryBudgets, float totalPlannedBudget, float totalCost) {
        this.plannedSubcategoryBudgets = plannedSubcategoryBudgets;
        this.totalPlannedBudget = totalPlannedBudget;
        this.totalCost = totalCost;
    }

    public ArrayList<PlannedSubcategoryBudget> getPlannedSubcategoryBudgets() {
        return plannedSubcategoryBudgets;
    }

    public void setPlannedSubcategoryBudgets(ArrayList<PlannedSubcategoryBudget> plannedSubcategoryBudgets) {
        this.plannedSubcategoryBudgets = plannedSubcategoryBudgets;
    }

    public float getTotalPlannedBudget() {
        return totalPlannedBudget;
    }

    public void setTotalPlannedBudget(float totalPlannedBudget) {
        this.totalPlannedBudget = totalPlannedBudget;
    }

    public float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(float totalCost) {
        this.totalCost = totalCost;
    }

    public void addCostToSubcategory(String categoryId, String subcategoryId, String serviceId, String productId, float cost) {
        totalCost += cost;

        boolean foundExistingSubcategory = false;

        for (PlannedSubcategoryBudget plannedSubcategoryBudget : plannedSubcategoryBudgets) {
            if (plannedSubcategoryBudget.getSubcategoryId().equals(subcategoryId)) {
                SpentSubcategoryBudget spentSubcategoryBudget = new SpentSubcategoryBudget();
                spentSubcategoryBudget.setServiceId(serviceId);
                spentSubcategoryBudget.setProductId(productId);
                spentSubcategoryBudget.setCost(cost);

                plannedSubcategoryBudget.getSpentSubcategoryBudgets().add(spentSubcategoryBudget);

                foundExistingSubcategory = true;
                break;
            }
        }

        if (!foundExistingSubcategory) {
            PlannedSubcategoryBudget plannedSubcategoryBudget = new PlannedSubcategoryBudget();
            plannedSubcategoryBudget.setSubcategoryId(subcategoryId);
            plannedSubcategoryBudget.setPlannedBudget(0);
            plannedSubcategoryBudget.setCategoryId(categoryId);
            plannedSubcategoryBudget.setSpentSubcategoryBudgets(new ArrayList<>());

            SpentSubcategoryBudget spentSubcategoryBudget = new SpentSubcategoryBudget();
            spentSubcategoryBudget.setServiceId(serviceId);
            spentSubcategoryBudget.setProductId(productId);
            spentSubcategoryBudget.setCost(cost);

            plannedSubcategoryBudget.getSpentSubcategoryBudgets().add(spentSubcategoryBudget);

            plannedSubcategoryBudgets.add(plannedSubcategoryBudget);
        }
    }
}
