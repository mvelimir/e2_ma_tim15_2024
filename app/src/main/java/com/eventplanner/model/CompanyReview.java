package com.eventplanner.model;

import java.util.Date;

public class CompanyReview {
    private String id;
    private String companyId;
    private String organizerId;
    private String comment;
    // 1-5
    private int rating;
    private Date date;

    public CompanyReview() {
    }

    public CompanyReview(String id, String companyId, String organizerId, String comment, int rating, Date date) {
        this.id = id;
        this.companyId = companyId;
        this.organizerId = organizerId;
        this.comment = comment;
        this.rating = rating;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getOrganizerId() {
        return organizerId;
    }

    public void setOrganizerId(String organizerId) {
        this.organizerId = organizerId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
