package com.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;



public class Subcategory implements Parcelable{
    private String id;
    private String categoryId;
    private String name;
    private String description;
    private SubcategoryType type;

    public Subcategory() {
    }

    public Subcategory(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Subcategory(String id, String name, String description, SubcategoryType type, String categoryId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.categoryId = categoryId;
    }

    public Subcategory(String id, String name, String description, String categoryId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.categoryId = categoryId;
    }

    public Subcategory(String name, String description, SubcategoryType type) {
        this.name = name;
        this.description = description;
        this.type = type;
    }


    protected Subcategory(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readString();
        }
        name = in.readString();
        description = in.readString();
        if (in.readByte() == 0) {
            categoryId = null;
        } else {
            categoryId = in.readString();
        }
    }

    public static final Parcelable.Creator<Subcategory> CREATOR = new Creator<Subcategory>() {
        @Override
        public Subcategory createFromParcel(Parcel in) {
            return new Subcategory(in);
        }

        @Override
        public Subcategory[] newArray(int size) {
            return new Subcategory[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeString(id);
        }
        dest.writeString(name);
        dest.writeString(description);
        if (categoryId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeString(categoryId);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public SubcategoryType getType() {
        return type;
    }

    public void setType(SubcategoryType type) {
        this.type = type;
    }
}
