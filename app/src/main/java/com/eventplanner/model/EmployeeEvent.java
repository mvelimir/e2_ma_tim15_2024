package com.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.eventplanner.ui.employees.EmployeeEventListFragment;

import java.util.Date;

public class EmployeeEvent {
    private String id;
    private String name;
    private Date date;
    private Date from;
    private Date to;
    private EmployeeEventCategory category;

    private float eventBudget;
    private float remainingBudget;

    public float getRemainingBudget() {
        return remainingBudget;
    }

    public void setRemainingBudget(float remainingBudget) {
        this.remainingBudget = remainingBudget;
    }

    public float getEventBudget() {
        return eventBudget;
    }

    public void setEventBudget(float eventBudget) {
        this.eventBudget = eventBudget;
    }

    public EmployeeEvent(String id, String name, Date date, Date from, Date to, EmployeeEventCategory category, float eventBudget, float remainingBugdet) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.from = from;
        this.to = to;
        this.category = category;
        this.eventBudget = eventBudget;
        this.remainingBudget = remainingBugdet;
    }

    public EmployeeEvent() {

    }

    public EmployeeEvent(String id, String name, Date date, Date from, Date to, EmployeeEventCategory category) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.from = from;
        this.to = to;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public EmployeeEventCategory getCategory() {
        return category;
    }

    public void setCategory(EmployeeEventCategory category) {
        this.category = category;
    }

}
