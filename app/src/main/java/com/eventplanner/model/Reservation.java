package com.eventplanner.model;

import java.util.Date;

public class Reservation {
    private String id;
    private String companyId;
    // OD koji je napravio rezervaciju
    private String organizerId;
    // PUP-Z za kog je rezervacija vezana
    private String employeeId;
    // Null dok se ne potvrdi rezervacija
    private String employeeEventId;
    private String packageId;
    private String serviceId;
    private String eventId;
    private Date cancellationTimeLimit;
    private Date from;
    private Date to;
    private Date finishTime;
    private ReservationStatus status;

    public Reservation() {
    }

    public Reservation(String id, String companyId, String organizerId, String employeeId, String employeeEventId, String packageId, String serviceId, String eventId, Date cancellationTimeLimit, Date from, Date to, Date finishTime, ReservationStatus status) {
        this.id = id;
        this.companyId = id;
        this.organizerId = organizerId;
        this.employeeId = employeeId;
        this.employeeEventId = employeeEventId;
        this.packageId = packageId;
        this.serviceId = serviceId;
        this.eventId = eventId;
        this.cancellationTimeLimit = cancellationTimeLimit;
        this.from = from;
        this.to = to;
        this.finishTime = finishTime;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getOrganizerId() {
        return organizerId;
    }

    public void setOrganizerId(String organizerId) {
        this.organizerId = organizerId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeEventId() {
        return employeeEventId;
    }

    public void setEmployeeEventId(String employeeEventId) {
        this.employeeEventId = employeeEventId;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Date getCancellationTimeLimit() {
        return cancellationTimeLimit;
    }

    public void setCancellationTimeLimit(Date cancellationTimeLimit) {
        this.cancellationTimeLimit = cancellationTimeLimit;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public ReservationStatus getStatus() {
        return status;
    }

    public void setStatus(ReservationStatus status) {
        this.status = status;
    }

    public boolean isPartOfPackage() {
        return packageId != null;
    }
}
