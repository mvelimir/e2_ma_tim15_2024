package com.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Company implements Parcelable {
    String id;

    public Company() {
    }

    private String ownerId;

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    private String companyEmail;
    private String companyName;
    private String companyAddress;
    private String companyPhone;
    private String companyDescritption;
    private String companyPhoto;

    private String ownerName;
    private String ownerSurname;
    private String ownerMail;
    private Date registrationRequestDate;

    public Date getRegistrationRequestDate() {
        return registrationRequestDate;
    }

    public void setRegistrationRequestDate(Date registrationRequestDate) {
        this.registrationRequestDate = registrationRequestDate;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setOwnerSurname(String ownerSurname) {
        this.ownerSurname = ownerSurname;
    }

    public void setOwnerMail(String ownerMail) {
        this.ownerMail = ownerMail;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getOwnerSurname() {
        return ownerSurname;
    }

    public String getOwnerMail() {
        return ownerMail;
    }

    private boolean IsRegistrationConfirmed;

    public boolean isRegistrationConfirmed() {
        return IsRegistrationConfirmed;
    }

    public void setRegistrationConfirmed(boolean registrationConfirmed) {
        IsRegistrationConfirmed = registrationConfirmed;
    }

    public Company(String id, String companyEmail, String companyName, String companyAddress, String companyPhone, String companyDescritption, String companyPhoto, String ownerId, String ownerName, String ownerSurname, String ownerMail, boolean IsRegistrationConfirmed, Date registrationRequestDate) {
        this.id = id;
        this.companyEmail = companyEmail;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.companyPhone = companyPhone;
        this.companyDescritption = companyDescritption;
        this.companyPhoto = companyPhoto;
        this.ownerId = ownerId;
        this.ownerName = ownerName;
        this.ownerSurname = ownerSurname;
        this.ownerMail = ownerMail;
        this.IsRegistrationConfirmed = IsRegistrationConfirmed;
        this.registrationRequestDate = registrationRequestDate;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public void setCompanyDescritption(String companyDescritption) {
        this.companyDescritption = companyDescritption;
    }

    public void setCompanyPhoto(String companyPhoto) {
        this.companyPhoto = companyPhoto;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public String getCompanyDescritption() {
        return companyDescritption;
    }

    public String getCompanyPhoto() {
        return companyPhoto;
    }

    public Company(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected Company(Parcel in) {
        id = in.readString();
        ownerId = in.readString();
        companyEmail = in.readString();
        companyName = in.readString();
        companyAddress = in.readString();
        companyPhone = in.readString();
        companyDescritption = in.readString();
        companyPhoto = in.readString();
        ownerName = in.readString();
        ownerSurname = in.readString();
        ownerMail = in.readString();
        IsRegistrationConfirmed = in.readByte() != 0;
        registrationRequestDate = new Date(in.readLong());
    }

    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel in) {
            return new Company(in);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(ownerId);
        dest.writeString(companyEmail);
        dest.writeString(companyName);
        dest.writeString(companyAddress);
        dest.writeString(companyPhone);
        dest.writeString(companyDescritption);
        dest.writeString(companyPhoto);
        dest.writeString(ownerName);
        dest.writeString(ownerSurname);
        dest.writeString(ownerMail);
        dest.writeByte((byte) (IsRegistrationConfirmed ? 1 : 0));
        dest.writeLong(registrationRequestDate != null ? registrationRequestDate.getTime() : -1);
    }
}
