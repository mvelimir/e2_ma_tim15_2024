package com.eventplanner.model;

public enum EmployeeEventCategory {
    RESERVED,
    BUSY
}
