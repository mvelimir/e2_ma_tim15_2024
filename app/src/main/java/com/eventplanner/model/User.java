package com.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Date;

public class User {
    protected String id;
    protected String email;
    protected String password;
    protected String firstName;
    protected String lastName;
    protected String homeAddress;
    protected String phoneNumber;
    protected String image;
    protected UserType type;

    protected Date registrationDate;

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setRegistrationConfirmed(boolean registrationConfirmed) {
        IsRegistrationConfirmed = registrationConfirmed;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public boolean isRegistrationConfirmed() {
        return IsRegistrationConfirmed;
    }

    protected boolean IsRegistrationConfirmed;


    public User() {
        super();
    }

    public User(String id, String email, String password, String firstName, String lastName, String homeAddress, String phoneNumber, String image, UserType type, boolean isRegistrationConfirmed/*, Date registrationDate*/) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.homeAddress = homeAddress;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.type = type;
        this.IsRegistrationConfirmed = false;
        //this.registrationDate = registrationDate;
    }



    public User(String email, String password, String name, String surname, String address, String phone){
        this.email = email;
        this.password = password;
        this.firstName = name;
        this.lastName = surname;
        this.homeAddress = address;
        this.phoneNumber = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }
}
