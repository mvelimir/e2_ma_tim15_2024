package com.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class EventType implements Parcelable {
    private Long Id;
    private String name;
    private String description;
    private boolean isActive;
    //private List<SubcategorySuggestion> subcategorySuggestions;

    public EventType() {
    }

    /*public EventType(Long id, String name, String description, boolean isActive, List<SubcategorySuggestion> subcategorySuggestions) {
        Id = id;
        this.name = name;
        this.description = description;
        this.isActive = isActive;
        //this.subcategorySuggestions = subcategorySuggestions;
    }*/

    public EventType(Long id, String name, String description, boolean isActive) {
        Id = id;
        this.name = name;
        this.description = description;
        this.isActive = isActive;
    }

    public EventType(Long id, String name, String description) {
        Id = id;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    protected EventType(Parcel in) {
        name = in.readString();
        description = in.readString();
        byte tmpIsDeactivated = in.readByte();
        if (in.readByte() == 0) {
            Id = null;
        } else {
            Id = in.readLong();
        }
    }

    public static final Creator<EventType> CREATOR = new Creator<EventType>() {
        @Override
        public EventType createFromParcel(Parcel in) {
            return new EventType(in);
        }

        @Override
        public EventType[] newArray(int size) {
            return new EventType[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        if (Id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(Id);
        }
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    /*public List<SubcategorySuggestion> getSubcategorySuggestions() {
        return subcategorySuggestions;
    }

    public void setSubcategorySuggestions(List<SubcategorySuggestion> subcategorySuggestions) {
        this.subcategorySuggestions = subcategorySuggestions;
    }*/
}

