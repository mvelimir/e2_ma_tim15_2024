package com.eventplanner.model;

import java.util.Date;

public class CompanyReviewReport {
    private String id;
    private String reviewId;
    private String ownerId;
    private Date date;
    private String reason;
    private CompanyReviewReportStatus status;

    public CompanyReviewReport() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public CompanyReviewReportStatus getStatus() {
        return status;
    }

    public void setStatus(CompanyReviewReportStatus status) {
        this.status = status;
    }
}
